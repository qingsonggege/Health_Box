//----------------------------------------------------
// Copyright (c) 2018, Djyos Open source Development team. All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. As a constituent part of djyos,do not transplant it to other software
//    without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2018，著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下三条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、本条件列表，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、本条件列表，以及下述
//    的免责声明。
// 3. 本软件作为都江堰操作系统的组成部分，未获事前取得的书面许可，不允许移植到非
//    都江堰操作系统环境下运行。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
//所属模块: GDD
//作者:  zhb.
//版本：V1.0.0
//文件描述: 虚拟键盘控件实现
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2016-10-08
//   作者:  zhb.
//   新版本号：V1.0.0
//   修改说明: 原始版本
//---------------------------------

#include  "gdd.h"
#include  <gui/gdd/gdd_private.h>
#include "font.h"
#include    <gdd_widget.h>
#include "dbug.h"
//static u8 gs_KeyBoardRowsDef=4;
//static u8 gs_KeyBoardColsDef=4;
//static u8 gs_KeySpaceDef=0;
//static u8 gs_KeyVtimeLimitDef=10;
////static char *gs_KeyValSetsDef="123C456Ｘ789O000.R";
//
//static u8 gs_KeyBoardRows;
//static u8 gs_KeyBoardCols;
//static u8 gs_KeySpace;
//static u8 gs_KeyWidth,gs_KeyHeight;
//
//static bool_t gs_bKeyDownFlag=false;
//static char gs_CurKeyDown;
//static s32 gs_KeyBoardId=0;
//
//
//
//
//
static u8 display_type=0;
static u8 KeyBoard_type=0;

extern HWND Get_TextBox_hwnd(void);
//界面元素信息
struct KEYINFO
{
    RECT position;//坐标，大小信息
//    const char* name;
    ptu32_t userParam;
} ;

//数字、字符元素定义
enum KeyBoard{
    ID_KEY_1,
    ID_KEY_2,
    ID_KEY_3,
    ID_KEY_4,
    ID_KEY_5,
    ID_KEY_6,
    ID_KEY_7,
    ID_KEY_8,
    ID_KEY_9,
    ID_KEY_10,
    ID_KEY_11,
    ID_KEY_12,
    ID_KEY_13,
    ID_KEY_14,
    ID_KEY_15,
    ID_KEY_16,
    ID_KEY_17,
    ID_KEY_18,
    ID_KEY_19,
    ID_KEY_20,
    ID_KEY_21,
    ID_KEY_22,
    ID_KEY_23,
    ID_KEY_24,
    ID_KEY_25,
    ID_KEY_26,
    ID_KEY_27,
    ID_KEY_28,
    ID_KEY_29,
    ID_KEY_30,
    ID_KEY_31,
    KEY_BACKGROUND,
    ENUM_MAXNUM,//总数量
};

const char *Displayname1[2][30] = {
{"1","2","3","4","5","6","7","8","9","0","-","+",":",";","(",")","$","&","@","#","#+=",".",",","?","!","'","←","abc","space","确定"},
{"[","]","{","}","#","%","^","*","+","=","_","(",")","~","<",">",":",";","&","@",".123",".",",","?","!","'","←","abc","space","确定"}
};

const char *Displayname2[2][31] = {
{"q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","↑","z","x","c","v","b","n","m","←",".123","space","确定"},
{"Q","W","E","R","T","Y","U","I","O","P","A","S","D","F","G","H","J","K","L","↑","Z","X","C","V","B","N","M","←",".123","space","确定"},
};

//==================================数字、符号======================================
static struct KEYINFO KeyBoardCfgTab1[ENUM_MAXNUM] =
{
    [ID_KEY_1] = {
        .position = {5+30*0,5,5+30*0+27,5+34},
    },
    [ID_KEY_2] = {
        .position = {5+30*1,5,5+30*1+27,5+34},
    },
    [ID_KEY_3] = {
        .position = {5+30*2,5,5+30*2+27,5+34},
    },
    [ID_KEY_4] = {
        .position = {5+30*3,5,5+30*3+27,5+34},
    },
    [ID_KEY_5] = {
        .position = {5+30*4,5,5+30*4+27,5+34},
    },
    [ID_KEY_6] = {
        .position = {5+30*5,5,5+30*5+27,5+34},
    },
    [ID_KEY_7] = {
        .position = {5+30*6,5,5+30*6+27,5+34},
    },
    [ID_KEY_8] = {
        .position = {5+30*7,5,5+30*7+27,5+34},
    },
    [ID_KEY_9] = {
        .position = {5+30*8,5,5+30*8+27,5+34},
    },
    [ID_KEY_10] = {
        .position = {5+30*9,5,5+30*9+27,5+34},
    },
    [ID_KEY_11] = {
        .position = {5+30*0,5+40,5+30*0+27,5+40+34},
    },
    [ID_KEY_12] = {
        .position = {5+30*1,5+40,5+30*1+27,5+40+34},
    },
    [ID_KEY_13] = {
        .position = {5+30*2,5+40,5+30*2+27,5+40+34},
    },
    [ID_KEY_14] = {
        .position = {5+30*3,5+40,5+30*3+27,5+40+34},
    },
    [ID_KEY_15] = {
        .position = {5+30*4,5+40,5+30*4+27,5+40+34},
    },
    [ID_KEY_16] = {
        .position = {5+30*5,5+40,5+30*5+27,5+40+34},
    },
    [ID_KEY_17] = {
        .position = {5+30*6,5+40,5+30*6+27,5+40+34},
    },
    [ID_KEY_18] = {
        .position = {5+30*7,5+40,5+30*7+27,5+40+34},
    },
    [ID_KEY_19] = {
        .position = {5+30*8,5+40,5+30*8+27,5+40+34},
    },
    [ID_KEY_20] = {
        .position = {5+30*9,5+40,5+30*9+27,5+40+34},
    },
    [ID_KEY_21] = {
        .position = {5+0,3+82,5+40,3+82+34},
    },
    [ID_KEY_22] = {
        .position = {5+45,3+82,5+45+40,3+82+34},
    },
    [ID_KEY_23] = {
        .position = {5+88,3+82,5+88+40,3+82+34},
    },
    [ID_KEY_24] = {
        .position = {5+131,3+82,5+131+40,3+82+34},
    },
    [ID_KEY_25] = {
        .position = {5+174,3+82,5+174+40,3+82+34},
    },
    [ID_KEY_26] = {
        .position = {5+217,3+82,5+217+40,3+82+34},
    },
    [ID_KEY_27] = {
        .position = {5+261,3+82,5+261+40,3+82+34},
    },
    [ID_KEY_28] = {
        .position = {5+0,3+122,5+74,3+122+34},
    },
    [ID_KEY_29] = {
        .position = {5+75+2,3+122,5+75+151-2,3+122+34},
    },
    [ID_KEY_30] = {
        .position = {5+227,3+122,5+227+74,3+122+34},
    },
    [KEY_BACKGROUND] = {
        .position = {0,0,5+227+74,3+122+34},
    },
};

//==================================大小写字母======================================
static struct KEYINFO KeyBoardCfgTab2[ENUM_MAXNUM] =
{
    [ID_KEY_1] = {
        .position = {5+30*0,5,5+30*0+27,5+34},
    },
    [ID_KEY_2] = {
        .position = {5+30*1,5,5+30*1+27,5+34},
    },
    [ID_KEY_3] = {
        .position = {5+30*2,5,5+30*2+27,5+34},
    },
    [ID_KEY_4] = {
        .position = {5+30*3,5,5+30*3+27,5+34},
    },
    [ID_KEY_5] = {
        .position = {5+30*4,5,5+30*4+27,5+34},
    },
    [ID_KEY_6] = {
        .position = {5+30*5,5,5+30*5+27,5+34},
    },
    [ID_KEY_7] = {
        .position = {5+30*6,5,5+30*6+27,5+34},
    },
    [ID_KEY_8] = {
        .position = {5+30*7,5,5+30*7+27,5+34},
    },
    [ID_KEY_9] = {
        .position = {5+30*8,5,5+30*8+27,5+34},
    },
    [ID_KEY_10] = {
        .position = {5+30*9,5,5+30*9+27,5+34},
    },
    [ID_KEY_11] = {
        .position = {5+30*0+12,5+40,5+30*0+27+12,5+40+34},
    },
    [ID_KEY_12] = {
        .position = {5+30*1+12,5+40,5+30*1+27+12,5+40+34},
    },
    [ID_KEY_13] = {
        .position = {5+30*2+12,5+40,5+30*2+27+12,5+40+34},
    },
    [ID_KEY_14] = {
        .position = {5+30*3+12,5+40,5+30*3+27+12,5+40+34},
    },
    [ID_KEY_15] = {
        .position = {5+30*4+12,5+40,5+30*4+27+12,5+40+34},
    },
    [ID_KEY_16] = {
        .position = {5+30*5+12,5+40,5+30*5+27+12,5+40+34},
    },
    [ID_KEY_17] = {
        .position = {5+30*6+12,5+40,5+30*6+27+12,5+40+34},
    },
    [ID_KEY_18] = {
        .position = {5+30*7+12,5+40,5+30*7+27+12,5+40+34},
    },
    [ID_KEY_19] = {
        .position = {5+30*8+12,5+40,5+30*8+27+12,5+40+34},
    },
    [ID_KEY_20] = {
        .position = {5,3+82,5+40,3+82+34},
    },
    [ID_KEY_21] = {
        .position = {49+31*0,3+82,49+31*0+28,3+82+34},
    },
    [ID_KEY_22] = {
        .position = {49+31*1,3+82,49+31*1+28,3+82+34},
    },
    [ID_KEY_23] = {
        .position = {49+31*2,3+82,49+31*2+28,3+82+34},
    },
    [ID_KEY_24] = {
        .position = {49+31*3,3+82,49+31*3+28,3+82+34},
    },
    [ID_KEY_25] = {
        .position = {49+31*4,3+82,49+31*4+28,3+82+34},
    },
    [ID_KEY_26] = {
        .position = {49+31*5,3+82,49+31*5+28,3+82+34},
    },
    [ID_KEY_27] = {
        .position = {49+31*6,3+82,49+31*6+28,3+82+34},
    },
    [ID_KEY_28] = {
        .position = {49+31*7,3+82,49+31*7+40,3+82+34},
    },
    [ID_KEY_29] = {
        .position = {5+0,3+122,5+74,3+122+34},
    },
    [ID_KEY_30] = {
        .position = {5+75+2,3+122,5+75+151-2,3+122+34},
    },
    [ID_KEY_31] = {
        .position = {5+227,3+122,5+227+74,3+122+34},
    },
    [KEY_BACKGROUND] = {
        .position = {0,0,5+227+74,3+122+34},
    },
};

static u16 keydown=ENUM_MAXNUM;  //按键点击的数字
static HWND KeyDowmHwnd;
void Update_KeyDowmHwnd(void)
{

    if(KeyDowmHwnd!=NULL)
    {
        GDD_PostMessage(KeyDowmHwnd,MSG_PAINT,0,0);
    }

}

static bool_t ViKeyBoard_Tonch(struct WindowMsg *pMsg)
{
    s16 x = LO16(pMsg->Param2);
    s16 y = HI16(pMsg->Param2);
    x=x-5;
    y=y-77;

    //==================================大小写字母======================================
    if(KeyBoard_type)
    {
        if(5<=y && y<=(5+34))
        {
            if((5+30*0)<=x && x<=(5+30*0+27))
            {
                keydown=ID_KEY_1;
            }
            else if((5+30*1)<=x && x<=(5+30*1+27))
            {
                keydown=ID_KEY_2;
            }
            else if((5+30*2)<=x && x<=(5+30*2+27))
            {
                keydown=ID_KEY_3;
            }
            else if((5+30*3)<=x && x<=(5+30*3+27))
            {
                keydown=ID_KEY_4;
            }
            else if((5+30*4)<=x && x<=(5+30*4+27))
            {
                keydown=ID_KEY_5;
            }
            else if((5+30*5)<=x && x<=(5+30*5+27))
            {
                keydown=ID_KEY_6;
            }
            else if((5+30*6)<=x && x<=(5+30*6+27))
            {
                keydown=ID_KEY_7;
            }
            else if((5+30*7)<=x && x<=(5+30*7+27))
            {
                keydown=ID_KEY_8;
            }
            else if((5+30*8)<=x && x<=(5+30*8+27))
            {
                keydown=ID_KEY_9;
            }
            else if((5+30*9)<=x && x<=(5+30*9+27))
            {
                keydown=ID_KEY_10;
            }
        }
        else if((5+40)<=y && y<=(5+40+34))
        {
            if((5+30*0+12)<=x && x<=(5+30*0+27+12))
            {
                keydown=ID_KEY_11;
            }
            else if((5+30*1+12)<=x && x<=(5+30*1+27+12))
            {
                keydown=ID_KEY_12;
            }
            else if((5+30*2+12)<=x && x<=(5+30*2+27+12))
            {
                keydown=ID_KEY_13;
            }
            else if((5+30*3+12)<=x && x<=(5+30*3+27+12))
            {
                keydown=ID_KEY_14;
            }
            else if((5+30*4+12)<=x && x<=(5+30*4+27+12))
            {
                keydown=ID_KEY_15;
            }
            else if((5+30*5+12)<=x && x<=(5+30*5+27+12))
            {
                keydown=ID_KEY_16;
            }
            else if((5+30*6+12)<=x && x<=(5+30*6+27+12))
            {
                keydown=ID_KEY_17;
            }
            else if((5+30*7+12)<=x && x<=(5+30*7+27+12))
            {
                keydown=ID_KEY_18;
            }
            else if((5+30*8+12)<=x && x<=(5+30*8+27+12))
            {
                keydown=ID_KEY_19;
            }
        }
        else if((3+82)<=y && y<=(3+82+34))
        {
            if(5<=x && x<=(5+40))
            {
                keydown=ID_KEY_20;
            }
            else if((49+31*0)<=x && x<=(49+31*0+28))
            {
                keydown=ID_KEY_21;
            }
            else if((49+31*1)<=x && x<=(49+31*1+28))
            {
                keydown=ID_KEY_22;
            }
            else if((49+31*2)<=x && x<=(49+31*2+28))
            {
                keydown=ID_KEY_23;
            }
            else if((49+31*3)<=x && x<=(49+31*3+28))
            {
                keydown=ID_KEY_24;
            }
            else if((49+31*4)<=x && x<=(49+31*4+28))
            {
                keydown=ID_KEY_25;
            }
            else if((49+31*5)<=x && x<=(49+31*5+28))
            {
                keydown=ID_KEY_26;
            }
            else if((49+31*6)<=x && x<=(49+31*6+28))
            {
                keydown=ID_KEY_27;
            }
            else if((49+31*7)<=x && x<=(49+31*7+28))
            {
                keydown=ID_KEY_28;
            }
        }
        else if((3+122)<=y && y<=(3+122+34))
        {
            if((5+0)<=x && x<=(5+74))
            {
                keydown=ID_KEY_29;
            }
            else if((5+75+2)<=x && x<=(5+75+151-2))
            {
                keydown=ID_KEY_30;
            }
            else if((5+227)<=x && x<=(5+227+74))
            {
                keydown=ID_KEY_31;
            }
        }
        else
        {
            keydown=ENUM_MAXNUM;
        }
    }

    //==================================数字、符号======================================
    else
    {
        if(5<=y && y<=(5+34))
        {
            if((5+30*0)<=x && x<=(5+30*0+27))
            {
                keydown=ID_KEY_1;
            }
            else if((5+30*1)<=x && x<=(5+30*1+27))
            {
                keydown=ID_KEY_2;
            }
            else if((5+30*2)<=x && x<=(5+30*2+27))
            {
                keydown=ID_KEY_3;
            }
            else if((5+30*3)<=x && x<=(5+30*3+27))
            {
                keydown=ID_KEY_4;
            }
            else if((5+30*4)<=x && x<=(5+30*4+27))
            {
                keydown=ID_KEY_5;
            }
            else if((5+30*5)<=x && x<=(5+30*5+27))
            {
                keydown=ID_KEY_6;
            }
            else if((5+30*6)<=x && x<=(5+30*6+27))
            {
                keydown=ID_KEY_7;
            }
            else if((5+30*7)<=x && x<=(5+30*7+27))
            {
                keydown=ID_KEY_8;
            }
            else if((5+30*8)<=x && x<=(5+30*8+27))
            {
                keydown=ID_KEY_9;
            }
            else if((5+30*9)<=x && x<=(5+30*9+27))
            {
                keydown=ID_KEY_10;
            }
        }
        else if((5+40)<=y && y<=(5+40+34))
        {
            if((5+30*0)<=x && x<=(5+30*0+27))
            {
                keydown=ID_KEY_11;
            }
            else if((5+30*1)<=x && x<=(5+30*1+27))
            {
                keydown=ID_KEY_12;
            }
            else if((5+30*2)<=x && x<=(5+30*2+27))
            {
                keydown=ID_KEY_13;
            }
            else if((5+30*3)<=x && x<=(5+30*3+27))
            {
                keydown=ID_KEY_14;
            }
            else if((5+30*4)<=x && x<=(5+30*4+27))
            {
                keydown=ID_KEY_15;
            }
            else if((5+30*5)<=x && x<=(5+30*5+27))
            {
                keydown=ID_KEY_16;
            }
            else if((5+30*6)<=x && x<=(5+30*6+27))
            {
                keydown=ID_KEY_17;
            }
            else if((5+30*7)<=x && x<=(5+30*7+27))
            {
                keydown=ID_KEY_18;
            }
            else if((5+30*8)<=x && x<=(5+30*8+27))
            {
                keydown=ID_KEY_19;
            }
            else if((5+30*9)<=x && x<=(5+30*9+27))
            {
                keydown=ID_KEY_20;
            }
        }
        else if((3+82)<=y && y<=(3+82+34))
        {
            if(5<=x && x<=(5+40))
            {
                keydown=ID_KEY_21;
            }
            else if((5+45)<=x && x<=(5+45+40))
            {
                keydown=ID_KEY_22;
            }
            else if((5+88)<=x && x<=(5+88+40))
            {
                keydown=ID_KEY_23;
            }
            else if((5+131)<=x && x<=(5+131+40))
            {
                keydown=ID_KEY_24;
            }
            else if((5+174)<=x && x<=(5+174+40))
            {
                keydown=ID_KEY_25;
            }
            else if((5+217)<=x && x<=(5+217+40))
            {
                keydown=ID_KEY_26;
            }
            else if((5+261)<=x && x<=(5+261+40))
            {
                keydown=ID_KEY_27;
            }
        }
        else if((3+122)<=y && y<=(3+122+34))
        {
            if((5+0)<=x && x<=(5+74))
            {
                keydown=ID_KEY_28;
            }
            else if((5+75+2)<=x && x<=(5+75+151-2))
            {
                keydown=ID_KEY_29;
            }
            else if((5+227)<=x && x<=(5+227+74))
            {
                keydown=ID_KEY_30;
            }
        }
        else
        {
            keydown=ENUM_MAXNUM;
        }
    }
    Update_KeyDowmHwnd();
//    printf("=========keydown is %d\r\n",keydown);
    return true;
}

//----------------------------------------------------------------------------
//功能：虚拟键盘控件的MSG_PAINT消息响应函数
//参数：pMsg，窗口消息指针
//返回：成功返回true,失败则返回false。
//-----------------------------------------------------------------------------
static  bool_t ButtonKeyBoard_Paint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC hdc;
    RECT rc;

    if(pMsg==NULL)
        return false;
    hwnd=pMsg->hwnd;
    if(hwnd==NULL)
        return false;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc==NULL)
        return false;

    GDD_GetClientRect(hwnd,&rc);


    GDD_SetFillColor(hdc,RGB(255,255,255));
    GDD_FillRect(hdc,&rc);

    if(KeyBoard_type)
    {
        for(int i=ID_KEY_1;i<=ID_KEY_31;i++)
        {
            if(keydown == i)
            {
                GDD_SetFillColor(hdc,RGB(208,212,218));
                GDD_FillRect(hdc,&KeyBoardCfgTab2[i].position);
            }
            else
            {
                GDD_SetFillColor(hdc,RGB(255,255,255));
                GDD_FillRect(hdc,&KeyBoardCfgTab2[i].position);
            }
            GDD_SetDrawColor(hdc,RGB(115,115,115));
            GDD_DrawRect(hdc,&KeyBoardCfgTab2[i].position);
            GDD_SetTextColor(hdc,RGB(28,32,42));
            GDD_DrawText(hdc,Displayname2[display_type][i],-1,&KeyBoardCfgTab2[i].position,DT_VCENTER|DT_CENTER);
        }
    }
    else
    {
        for(int i=ID_KEY_1;i<=ID_KEY_30;i++)
        {
            if(keydown == i)
            {
                GDD_SetFillColor(hdc,RGB(208,212,218));
                GDD_FillRect(hdc,&KeyBoardCfgTab1[i].position);
            }
            else
            {
                GDD_SetFillColor(hdc,RGB(255,255,255));
                GDD_FillRect(hdc,&KeyBoardCfgTab1[i].position);
            }
            GDD_SetDrawColor(hdc,RGB(115,115,115));
            GDD_DrawRect(hdc,&KeyBoardCfgTab1[i].position);
            GDD_SetTextColor(hdc,RGB(28,32,42));
            GDD_DrawText(hdc,Displayname1[display_type][i],-1,&KeyBoardCfgTab1[i].position,DT_VCENTER|DT_CENTER);
        }
    }

    GDD_EndPaint(hwnd,hdc);

    return true;
}


//----------------------------------------------------------------------------
//功能：虚拟键盘控件的MSG_PAINT消息响应函数
//参数：pMsg，窗口消息指针
//返回：成功返回true,失败则返回false。
//-----------------------------------------------------------------------------
static  bool_t ViKeyBoard_Paint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC hdc;
    RECT rc;

    if(pMsg==NULL)
        return false;
    hwnd=pMsg->hwnd;
    if(hwnd==NULL)
        return false;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc==NULL)
        return false;
    GDD_GetClientRect(hwnd,&rc);
    GDD_SetFillColor(hdc,RGB(255,255,255));
    GDD_FillRect(hdc,&rc);

    GDD_EndPaint(hwnd,hdc);

    return true;
}

//---------------------------------------------------------------------------
//功能：虚拟按键创建消息响应函数
//参数：pMsg，窗口消息指针
//返回：成功返回true,失败则返回false。
//-----------------------------------------------------------------------------
static bool_t __Widget_VirKeyBoardCreate(struct WindowMsg *pMsg)
{
    HWND hwnd =pMsg->hwnd;

//    Mainlistcb.Firstmenu = true;
    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
            {MSG_PAINT,ButtonKeyBoard_Paint},
            {MSG_TOUCH_DOWN,ViKeyBoard_Tonch},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    KeyDowmHwnd=Widget_CreateButton("键盘按钮", WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
            KeyBoardCfgTab2[KEY_BACKGROUND].position.left, KeyBoardCfgTab2[KEY_BACKGROUND].position.top,\
                GDD_RectW(&KeyBoardCfgTab2[KEY_BACKGROUND].position),GDD_RectH(&KeyBoardCfgTab2[KEY_BACKGROUND].position), //按钮位置和大小
                hwnd,KEY_BACKGROUND,(ptu32_t)Displayname2[display_type][KEY_BACKGROUND],&s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据

    return true;
}


//-----------------------------------------------------------------
//功能：虚拟键盘控件MSG_NOTIFY消息响应函数
 //参数：pMsg，窗口消息指针。
 //返回：成功返回true,失败则返回false。
//-----------------------------------------------------------------------------
static bool_t __Widget_VirKeyBoardNotifyHandle(struct WindowMsg *pMsg)
{
    u16 event =HI16(pMsg->Param1);
    u16 id =LO16(pMsg->Param1);
    HWND focus_hwnd=GDD_GetFocusWindow();

    focus_hwnd=GDD_GetFocusWindow();

    if(event==MSG_BTN_UP && keydown != ENUM_MAXNUM)
    {
        switch(id)
        {
            case KEY_BACKGROUND:
            if(KeyBoard_type)
            {
                if(ID_KEY_20==keydown || ID_KEY_29==keydown)
                {
                    if(ID_KEY_20==keydown)
                    {
                        if(display_type)
                        {
                            display_type= 0;
                        }
                        else
                        {
                            display_type= 1;
                        }
                    }
                    else if(ID_KEY_29==keydown)
                    {
                        KeyBoard_type =0;
                        keydown=ID_KEY_28;
                        display_type= 0;
                    }
                    GDD_PostMessage(Get_TextBox_hwnd(), MSG_TOUCH_DOWN, 1, 0);
                }
                else if(ID_KEY_31==keydown)
                {
                    GDD_PostMessage(Get_TextBox_hwnd(), MSG_TOUCH_DOWN, 0, 0);
                }
                else if(ID_KEY_28==keydown)
                {
                    GDD_PostMessage( focus_hwnd, MSG_KEY_DOWN, VK_DEL_CHAR, 0);
                }
                else if(ID_KEY_30!=keydown)
                {
                    GDD_PostMessage( focus_hwnd, MSG_KEY_DOWN, (u32)*Displayname2[display_type][keydown], 0);
                }
            }
            else
            {
                if(ID_KEY_21==keydown || ID_KEY_28==keydown )
                {
                    if(ID_KEY_21==keydown)
                    {
                        if(display_type)
                        {
                            display_type= 0;
                        }
                        else
                        {
                            display_type= 1;
                        }

                    }
                    else if(ID_KEY_28==keydown)
                    {
                        KeyBoard_type =1;
                        keydown=ID_KEY_29;
                        display_type= 0;
                    }
                    GDD_PostMessage(Get_TextBox_hwnd(), MSG_TOUCH_DOWN, 1, 0);
                }
                else if(ID_KEY_30==keydown)
                {
                    GDD_PostMessage(Get_TextBox_hwnd(), MSG_TOUCH_DOWN, 0, 0);
                }
                else if(ID_KEY_27==keydown)
                {
                    GDD_PostMessage( focus_hwnd, MSG_KEY_DOWN, VK_DEL_CHAR, 0);
                }
                else if(ID_KEY_29!=keydown)
                {
                    GDD_PostMessage( focus_hwnd, MSG_KEY_DOWN, (u32)*Displayname1[display_type][keydown], 0);
                }
            }
            keydown=ENUM_MAXNUM;
            break;
            default:
                  break;
        }

    }
    return true;
}



//默认虚拟键盘消息处理函数表，处理用户函数表中没有处理的消息。
static struct MsgProcTable s_gVirKeyBoardMsgProcTable[] =
{
    {MSG_CREATE,__Widget_VirKeyBoardCreate},
    {MSG_PAINT,ViKeyBoard_Paint},
    {MSG_NOTIFY,__Widget_VirKeyBoardNotifyHandle},
};

static struct MsgTableLink  s_gVirKeyBoardMsgLink;
// =============================================================================
// 函数功能: 虚拟键盘控件创建函数。
// 输入参数: Text:虚拟键盘窗口Text;
//           Style:虚拟键盘风格，参见gdd.h;
//           x:虚拟键盘起始位置x方向坐标(单位：像素);
//           y:虚拟键盘起始位置y方向坐标(单位：像素);
//           w:虚拟键盘宽度(单位：像素);
//           h:虚拟键盘高度(单位：像素);
//           hParent:虚拟键盘父窗口句柄;
//           WinId:虚拟键盘控件Id;
//           pdata:虚拟键盘控件私有数据结构;
//           UserMsgTableLink:虚拟键盘控件用户消息列表结构指针。
// 输出参数: 无。
// 返回值  :成功则返回文本框句柄，失败则返回NULL。
// =============================================================================
HWND Widget_CreateVirKeyBoard(const char *Text,u32 Style,
                    s32 x,s32 y,s32 w,s32 h,
                    HWND hParent,u32 WinId,ptu32_t pdata,
                    struct MsgTableLink *UserMsgTableLink)
{
    HWND pGddWin;
    s_gVirKeyBoardMsgLink.MsgNum = sizeof(s_gVirKeyBoardMsgProcTable) / sizeof(struct MsgProcTable);
    s_gVirKeyBoardMsgLink.myTable = (struct MsgProcTable *)&s_gVirKeyBoardMsgProcTable;
    pGddWin=GDD_CreateWindow(Text,WS_CHILD | WS_CAN_FOCUS|Style,x,y,w,h,hParent,WinId,
                            CN_WINBUF_PARENT,pdata,&s_gVirKeyBoardMsgLink);
    if(UserMsgTableLink != NULL)
        GDD_AddProcFuncTable(pGddWin,UserMsgTableLink);
    return pGddWin;
}



