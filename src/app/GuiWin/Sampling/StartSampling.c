//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "qrencode.h"
#include "board.h"
#include "../inc/ServerEvent.h"
//界面元素定义
enum StartSampling{
    StartSampling_BACKGROUND, //背景
    StartSampling_Line,
    StartSampling_Logo,
    StartSampling_WIFI,       //wifi
    StartSampling_POWER_LOGO,
    StartSampling_BACK,      //返回
    StartSampling_Progress1,
    StartSampling_Progress2,
    StartSampling_TEXT,      //
    StartSampling_Stopampling,    //停止采样
    ENUM_MAXNUM,//总数量
};

//==================================config======================================
static struct GUIINFO StartSamplingCfgTab[ENUM_MAXNUM] =
{
    [StartSampling_BACKGROUND] = {
        .position = {0,0,320,240},
        .name = "Background",
        .type = type_background,
        .userParam = RGB(255,253,235),
    },
    [StartSampling_Line] = {
        .position = {8,31,8+303,31+2},
        .name = "DividingLine",
        .type = widget_type_Line,
        .userParam = RGB(163,162,154),//划线的颜色
    },
    [StartSampling_BACK] = {
        .position ={0,0,80,30},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_back_bmp,
    },
    [StartSampling_Logo] = {
        .position = {287,5,287+25,5+24},
        .name = "量子",
        .type = widget_type_picture,
        .userParam = BMP_Quantum_bmp,
    },
    [StartSampling_WIFI] = {
        .position = {265,8,265+17,8+19},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },
    [StartSampling_POWER_LOGO] = {
        .position = {233,12,233+23,12+13},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },
    [StartSampling_Progress1] = {
        .position = {106-34,62,106+110,62+47},
        .name = "数字进度",
        .type = widget_type_Progress,
        .userParam = 0,
    },
    [StartSampling_Progress2] = {
        .position = {114,125,114+18,125+18},
        .name = "滚动方块",
        .type = widget_type_Progress,
        .userParam = 0,
    },
    [StartSampling_TEXT] = {
        .position = {10,174,310,174+16},
        .name = "请保持手势直到检测结束！",
        .type = widget_type_text,
        .userParam = RGB(255,65,106),
    },
    [StartSampling_Stopampling] = {
        .position ={120,207,120+80,207+24},
        .name = "停止检测",
        .type = widget_type_button,
        .userParam = BMP_Stop_bmp,
    },
};

struct colorCB
{
   u8 first;
   u8 second;
   u8 third;
};
static struct colorCB colorcb = {2,1,0};

static size_t UartProgerss =0;

void Set_UartProgerss(size_t len)
{
//    if(len > 100)
//        return ;
    UartProgerss = len;
}

//按钮控件创建函数
static bool_t  StartSamplingButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,StartSamplingCfgTab[StartSampling_BACK].name))//返回
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,8,10,bmp);
            }
        }else if(0==strcmp(buttoninfo->name,StartSamplingCfgTab[StartSampling_Stopampling].name))
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,0,0,bmp);
            }
        }
        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_StartSampling(struct WindowMsg *pMsg)
{
//    RECT rc0;
    HWND hwnd =pMsg->hwnd;

    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, StartSamplingButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

//    GDD_GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (StartSamplingCfgTab[i].type)
        {
            case  widget_type_button :
                {
                 Widget_CreateButton(StartSamplingCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         StartSamplingCfgTab[i].position.left, StartSamplingCfgTab[i].position.top,\
                         GDD_RectW(&StartSamplingCfgTab[i].position),GDD_RectH(&StartSamplingCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&StartSamplingCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

enum Bmptype DisplayNumber(u8 time)
{
    enum Bmptype num_bmp;
    switch(time)
    {
        case 0:
            num_bmp = BMP_0_bmp;
        break;
        case 1:
            num_bmp = BMP_1_bmp;
        break;
        case 2:
            num_bmp = BMP_2_bmp;
        break;
        case 3:
            num_bmp = BMP_3_bmp;
        break;
        case 4:
            num_bmp = BMP_4_bmp;
        break;
        case 5:
            num_bmp = BMP_5_bmp;
        break;
        case 6:
            num_bmp = BMP_6_bmp;
        break;
        case 7:
            num_bmp = BMP_7_bmp;
        break;
        case 8:
            num_bmp = BMP_8_bmp;
        break;
        case 9:
            num_bmp = BMP_9_bmp;
        break;
        default:
            num_bmp = Bmp_NULL;
        break;
    }
    return num_bmp;
}

u32 SetRectColor(int ret)
{
    u32 color;
    switch(ret)
    {
        case 0:
            color =  RGB(0,170,241);
            break;
        case 1:
            color =  RGB(184,234,255);
            break;
        case 2:
            color =  RGB(213,243,255);
            break;
    default: break;
    }
   return color;
}

//绘制消息处函数
static bool_t HmiPaint_StartSampling(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp ;
//    struct WinTime time;
    hwnd =pMsg->hwnd;
//    char  buf[10];
//    int  Get_DeviceSn(char *buf);
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (StartSamplingCfgTab[i].type)
            {
                case  type_background :
                    GDD_SetFillColor(hdc,StartSamplingCfgTab[i].userParam);
                    GDD_FillRect(hdc,&StartSamplingCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(StartSamplingCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,StartSamplingCfgTab[i].position.left,\
                                StartSamplingCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_Power :
                    if(IsCharge() == true)
                    {
                        bmp = Get_BmpBuf(BMP_Powering_bmp);
                        if(bmp != NULL)
                        {
                            GDD_DrawBMP(hdc,StartSamplingCfgTab[i].position.left-7,\
                                    StartSamplingCfgTab[i].position.top,bmp);
                        }
                    }
                    bmp = Get_BmpBuf(StartSamplingCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,StartSamplingCfgTab[i].position.left,\
                                StartSamplingCfgTab[i].position.top,bmp);
                    }
                break;

                case  widget_type_Line :
                    GDD_SetFillColor(hdc,StartSamplingCfgTab[i].userParam);
                    GDD_FillRect(hdc,&StartSamplingCfgTab[i].position);

                    GDD_SetDrawColor(hdc,RGB(115,115,115));
                    GDD_DrawLine(hdc,StartSamplingCfgTab[i].position.left+20,StartSamplingCfgTab[i].position.top+163,\
                            StartSamplingCfgTab[i].position.right-20,StartSamplingCfgTab[i].position.bottom+161); //L
                    break;
                case  widget_type_text :
                    GDD_SetTextColor(hdc,StartSamplingCfgTab[i].userParam);
                    GDD_DrawText(hdc,StartSamplingCfgTab[i].name,-1,&StartSamplingCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;
                case  widget_type_Progress :
                    if(i == StartSampling_Progress1)
                    {
                        if(UartProgerss == 100)
                        {
                            bmp = Get_BmpBuf(DisplayNumber(1));
                            GDD_DrawBMP(hdc,StartSamplingCfgTab[i].position.left,\
                                    StartSamplingCfgTab[i].position.top,bmp);
                        }
                        bmp = Get_BmpBuf(DisplayNumber(UartProgerss%100/10));
                        GDD_DrawBMP(hdc,StartSamplingCfgTab[i].position.left+36,\
                                StartSamplingCfgTab[i].position.top,bmp);

                        bmp = Get_BmpBuf(DisplayNumber(UartProgerss%100%10));
                        GDD_DrawBMP(hdc,StartSamplingCfgTab[i].position.left+36+36,\
                                StartSamplingCfgTab[i].position.top,bmp);

                        bmp = Get_BmpBuf(BMP_10_bmp);
                        GDD_DrawBMP(hdc,StartSamplingCfgTab[i].position.left+36+36+36,\
                                StartSamplingCfgTab[i].position.top,bmp);
                    }else{
                        GDD_SetRect(&rc,StartSamplingCfgTab[i].position.left,StartSamplingCfgTab[i].position.top,\
                                GDD_RectW(&StartSamplingCfgTab[i].position),GDD_RectH(&StartSamplingCfgTab[i].position));

                        GDD_SetFillColor(hdc,SetRectColor(colorcb.first));
                        GDD_FillRect(hdc,&rc);
                        GDD_OffsetRect(&rc,36, 0);
                        GDD_SetFillColor(hdc,SetRectColor(colorcb.second));
                        GDD_FillRect(hdc,&rc);
                        GDD_OffsetRect(&rc,36, 0);
                        GDD_SetFillColor(hdc,SetRectColor(colorcb.third));
                        GDD_FillRect(hdc,&rc);

                        colorcb.first++;
                        colorcb.second++;
                        colorcb.third++;
                        if(colorcb.first > 2)
                            colorcb.first = 0;
                        if(colorcb.second > 2)
                            colorcb.second = 0;
                        if(colorcb.third > 2)
                            colorcb.third = 0;
                    }

                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

//子控件发来的通知消息
static enum WinType  HmiNotify_StartSampling(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case  StartSampling_BACK        :
                Set_Detect_Time();
                DisableSampling();
                NextUi = Win_ReadSampling;
                break;
            case  StartSampling_Stopampling:
                DisableSampling();
                NextUi = WIN_Main_WIN;
                break;

            default: break;
        }
    }
    return NextUi;
}

int Register_StartSampling()
{
    return Register_NewWin(Win_StartSampling,HmiCreate_StartSampling,HmiPaint_StartSampling,HmiNotify_StartSampling);

}

