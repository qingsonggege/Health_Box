//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "qrencode.h"
#include "../inc/ServerEvent.h"
//界面元素定义
enum DrencodeLogin{
    DrencodeLogin_BACKGROUND, //背景
    DrencodeLogin_Line,
    DrencodeLogin_WIFI,       //wifi
    DrencodeLogin_POWER_LOGO,//
    DrencodeLogin_BACK,      //返回
    DrencodeLogin_Logo,      //
    DrencodeLogin_Qrencode,      //二维码
    DrencodeLogin_Sampling,      //准备采样
    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static struct GUIINFO DrencodeLoginCfgTab[ENUM_MAXNUM] =
{
    [DrencodeLogin_BACKGROUND] = {
        .position = {0,0,320,240},
        .name = "Background",
        .type = type_background,
        .userParam = RGB(255,253,235),
    },
    [DrencodeLogin_Line] = {
        .position = {8,31,8+303,31+2},
        .name = "DividingLine",
        .type = widget_type_Line,
        .userParam = RGB(163,162,154),//划线的颜色
    },
    [DrencodeLogin_BACK] = {
        .position ={0,0,80,30},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_back_bmp,
    },
    [DrencodeLogin_WIFI] = {
        .position = {265,8,265+17,8+19},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },
    [DrencodeLogin_POWER_LOGO] = {
        .position = {233,12,233+23,12+13},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },
    [DrencodeLogin_Logo] = {
        .position = {287,5,287+25,5+24},
        .name = "量子",
        .type = widget_type_picture,
        .userParam = BMP_Quantum_bmp,
    },
    [DrencodeLogin_Qrencode] = {
        .position = {0,50,320,50+100},
        .name = "qrencode",
        .type = widget_type_Orencode,
        .userParam = 0,
    },
    [DrencodeLogin_Sampling] = {
        .position ={54,174,54+220,174+49},
        .name = "点击采样",
        .type = widget_type_button,
        .userParam = BMP_Click_bmp,
    },
};

extern enumServerStatue  ServerStatue_key;

//按钮控件创建函数
static bool_t  DrencodeLoginButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

//        GDD_SetFillColor(hdc,RGB(255,253,235));
//        GDD_FillRect(hdc,&rc);

        if(0==strcmp(buttoninfo->name,DrencodeLoginCfgTab[DrencodeLogin_BACK].name))//返回
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,8,10,bmp);
            }
        }else if(0==strcmp(buttoninfo->name,DrencodeLoginCfgTab[DrencodeLogin_Sampling].name))//返回
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,1,0,bmp);
            }
        }
        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_DrencodeLogin(struct WindowMsg *pMsg)
{
//    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, DrencodeLoginButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

//    GDD_GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (DrencodeLoginCfgTab[i].type)
        {
            case  widget_type_button :
                {
                 Widget_CreateButton(DrencodeLoginCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         DrencodeLoginCfgTab[i].position.left, DrencodeLoginCfgTab[i].position.top,\
                         GDD_RectW(&DrencodeLoginCfgTab[i].position),GDD_RectH(&DrencodeLoginCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&DrencodeLoginCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

/*---------------------------------------------------------------------------
功能： 将从二维码函数库中得到的数据转换为指向存储像素阵列的数组
    输入   :QRcode结构体指针
    输出 :存储像素阵列的数组指针
---------------------------------------------------------------------------*/
static u8 *Data_conversion(QRcode*qrcode,u8 byte)
{
    int i,j,k,n,m,w;
    u8*Data;
    u8*QrcodeData;
    u8 *addr;
    QrcodeData=qrcode->data;
    n=((byte*qrcode->width)+7)/8;   //每行占得字节数
    addr=(u8*)malloc(n*(qrcode->width*byte));
    if(addr == NULL)
        return NULL;
    Data = addr;
    w = (qrcode->width)/8;
    m = (qrcode->width)%8;
#define switch_flag  0

    for(k = 0;k < qrcode->width ; k++)
    {
        u32 num = 0;
        for(i=0;i<w;i++)//复制字节整数
        {
            for(j=0;j<8;j++)
            {
                if((*QrcodeData)&0x01)
                {
                    for(u8 cnt =0;cnt<byte;cnt++)
                    {
#if switch_flag
                        Data[num/8] |= (0x80>>num%8);
#else
                        Data[num/8] &= ~(0x80>>num%8);
#endif
                        num++;
                    }
                }
                else
                {
                    for(u8 cnt =0;cnt<byte;cnt++)
                    {
#if switch_flag
                        Data[num/8] &= ~(0x80>>num%8);
#else
                        Data[num/8] |= (0x80>>num%8);
#endif
                        num++;
                    }
                }
                QrcodeData++;
            }
        }
        if(m != 0)
        for(j=0;j<m;j++)//复制字节余数部分
        {
            if((*QrcodeData)&0x01)
            {
                for(u8 cnt =0;cnt<byte;cnt++)
                {
#if switch_flag
                        Data[num/8] |= (0x80>>num%8);
#else
                        Data[num/8] &= ~(0x80>>num%8);
#endif
                    num++;
                }
            }
            else
            {
                for(u8 cnt =0;cnt<byte;cnt++)
                {
#if switch_flag
                        Data[num/8] &= ~(0x80>>num%8);
#else
                        Data[num/8] |= (0x80>>num%8);
#endif
                    num++;
                }
            }
            QrcodeData++;
        }
        for(int cnt=1;cnt<byte;cnt++)
        {
            memcpy(Data+cnt*n,Data,n);
        }
        Data += n * byte;
    }
    return addr;
}
/*---------------------------------------------------------------------------
功能：根据输入参数得到二维码信息
    输入   :string: 被编码的数据
        version: 版本号<=40     宽度=(版本号*4+17)
        level: 容错等级L（Low）：7%的字码可被修正
               M（Medium）：15%的字码可被修正
               Q（Quartile）：25%的字码可被修正
               H（High）：30%的字码可被修正
        hint: 编码方式
        casesensitive: 是否区分大小写
    输出 :存储像素阵列的数组指针
---------------------------------------------------------------------------*/
extern QRcode *QRcode_encodeString(const char *string, int version,
                                    QRecLevel level, QRencodeMode hint,
                                    int casesensitive);

//绘制消息处函数
static bool_t HmiPaint_Qrcode(struct WindowMsg *pMsg,char *SNbuf,s32 x,s32 y,u8 byte)
{
    HWND hwnd;
    HDC  hdc;
//    RECT rc0;
    QRcode*qrcode; //最后结果
    struct RectBitmap   bitmap;
    struct RopGroup RopCode ={ 0, 0, 0, CN_R2_COPYPEN, 0, 0,0};
    u8 *Data;

    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
//    GDD_GetClientRect(hwnd,&rc0);


//    rc0=DrencodeLoginCfgTab[DrencodeLogin_Qrencode].position;
//
//    GDD_SetFillColor(hdc,RGB(247,192,51));
//    GDD_FillRect(hdc,&rc0);

    GDD_SetDrawColor(hdc,CN_COLOR_BLACK);
/* 89x89位宽的二维码参数为
 * 版本号:18
 * 容错等级  (ECC)  数字  (容量)     字母                                汉字          二进制代码
 * L            1,725       1,046       442    586
 * M            1,346       816         345    450
 * Q            948         574         243    322
 * H            746         452         191    250
 * */



    qrcode= QRcode_encodeString(SNbuf,5, QR_ECLEVEL_H, QR_MODE_8,1);
   if(qrcode==NULL)//存储空间不足或数据容量超出规定范围
   {
       QRcode_free(qrcode);
       return false;
   }

    Data=Data_conversion(qrcode,byte);//提取显示数据到Data
    bitmap.bm_bits = Data;
    bitmap.linebytes = ((byte*qrcode->width)+7)/8;
    bitmap.PixelFormat = CN_SYS_PF_GRAY1;
    bitmap.ExColor = RGB(255,253,235);
    bitmap.height=(s32)(qrcode->width*byte);
    bitmap.width=(s32)(qrcode->width*byte);
    printf("bitmap.height = %d,bitmap.width = %d",bitmap.height,bitmap.width);
    GDD_DrawBitmap(hdc,x,y,&bitmap,CN_SYS_PF_GRAY1,RopCode);

    GDD_EndPaint(hwnd,hdc);
    QRcode_free(qrcode);
    free(Data);
    return true;
}

//绘制消息处函数
static bool_t HmiPaint_DrencodeLogin(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
//    struct WinTime time;
    hwnd =pMsg->hwnd;
//    char  SNbuf[40];// = "12345678";
//    char  buf[10];
    int ret = 0;
//    int  Get_DeviceSn(char *buf);
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (DrencodeLoginCfgTab[i].type)
            {
                case  type_background :
                    GDD_SetFillColor(hdc,DrencodeLoginCfgTab[i].userParam);
                    GDD_FillRect(hdc,&DrencodeLoginCfgTab[i].position);
                    break;
                case  widget_type_picture :
                bmp = Get_BmpBuf(DrencodeLoginCfgTab[i].userParam);
                if(bmp != NULL)
                {
                    GDD_DrawBMP(hdc,DrencodeLoginCfgTab[i].position.left,\
                            DrencodeLoginCfgTab[i].position.top,bmp);
                }
                break;
                case  widget_type_Power :
                    if(IsCharge() == true)
                    {
                        bmp = Get_BmpBuf(BMP_Powering_bmp);
                        if(bmp != NULL)
                        {
                            GDD_DrawBMP(hdc,DrencodeLoginCfgTab[i].position.left-7,\
                                    DrencodeLoginCfgTab[i].position.top,bmp);
                        }
                    }
                    bmp = Get_BmpBuf(DrencodeLoginCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,DrencodeLoginCfgTab[i].position.left,\
                                DrencodeLoginCfgTab[i].position.top,bmp);
                    }
                break;
                case  widget_type_Line :
                    GDD_SetFillColor(hdc,DrencodeLoginCfgTab[i].userParam);
                    GDD_FillRect(hdc,&DrencodeLoginCfgTab[i].position);
                    break;
                case  widget_type_Orencode :
                    printf("Get_UserDeviceKey,Orencode\r\n");
//                    ret = Get_UserDeviceKey(SNbuf,sizeof(SNbuf),1);
                    if(ServerStatue_key == Get_Illegal){
                        printf("invalid :Get_DeviceKey ret:%d \r\n",ret);
                        GDD_SetTextColor(hdc,RGB(28,32,42));
                        GDD_DrawText(hdc,"设备尚未注册！",-1,&DrencodeLoginCfgTab[i].position,DT_VCENTER|DT_CENTER);
                    }else{
                        HmiPaint_Qrcode(pMsg,Get_UserDevidKey(),DrencodeLoginCfgTab[i].position.left+100,DrencodeLoginCfgTab[i].position.top,3);
                    }
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

//子控件发来的通知消息
static enum WinType  HmiNotify_DrencodeLogin(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);
//    int ret;
    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case  DrencodeLogin_BACK :
                NextUi = WIN_Main_WIN;
                break;
            case  DrencodeLogin_Sampling :
//                ret = Update_DeviceUid();
//                if(ret < 0)
//                {
//                    printf("error :Update_DeviceUid ret:%d \r\n",ret);
//                    NextUi = Win_ResetLogin;
//                }else{
//                    ret = Update_DeviceComputeOrder();
//                    if(ret < 0)  printf("error :Update_DeviceComputeOrder ret:%d \r\n",ret);
//                    printf("ENUM_NETWORK button");
//                    Set_Detect_Time();
//                    NextUi = Win_ReadSampling;
//                }
                Set_Detect_Time();
                Server_SendComd(cmd_Get_uidComputer);
                NextUi = Win_Netloading;
                break;
            default: break;
        }
    }
    return NextUi;
}

int Register_DrencodeLogin()
{
    return Register_NewWin(Win_DrencodeLogin,HmiCreate_DrencodeLogin,HmiPaint_DrencodeLogin,HmiNotify_DrencodeLogin);

}

