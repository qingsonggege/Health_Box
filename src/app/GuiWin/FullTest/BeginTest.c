//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include <upgrade.h>
#include "project_config.h"
#include "VoicePrompt.h"
#include "gpio_pub.h"
#include "cpu_peri.h"

//extern char start_user_loading;
//控件ID编号
enum BgeinTestId{
    BgeinTest_BACKGROUND, //背景
    BgeinTest_NetText,
    BgeinTest_VoiceText,
    BgeinTest_Restart,
    BgeinTest_Confirm,
    BgeinTest_UartText,
    BgeinTest_Hand,
    BgeinTest_TextFinish,
    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO BgeinTestCfgTab[ENUM_MAXNUM] =
{
    [BgeinTest_BACKGROUND] = {
        .position = {0,0,320,240},
        .name = "Background",
        .type = type_background,
        .userParam = RGB(255,253,235),
    },
    [BgeinTest_NetText] = {
        .position = {10,5,310,35},
        .name = "wifi联网测试",
        .type = widget_type_text,
        .userParam = RGB(115,115,115),
    },
    [BgeinTest_VoiceText] = {
        .position = {10,55,310,85},
        .name = "喇叭测试",
        .type = widget_type_text,
        .userParam = RGB(115,115,115),
    },
    [BgeinTest_Restart] = {
        .position = {130,55,180,85},
        .name = "播放",
        .type = widget_type_button,
        .userParam = RGB(255,0,0),
    },
    [BgeinTest_Confirm] = {
        .position = {200,55,250,85},
        .name = "确定",
        .type = widget_type_button,
        .userParam = RGB(255,0,0),
    },
    [BgeinTest_UartText] = {
        .position = {10,105,310,135},
        .name = "采集模块的收发测试",
        .type = widget_type_text,
        .userParam = RGB(115,115,115),
    },
    [BgeinTest_Hand] = {
        .position = {10,155,310,175},
        .name = "手势检测测试",
        .type = widget_type_text,
        .userParam = RGB(115,115,115),
    },
    [BgeinTest_TextFinish] = {
        .position = {100,205,100+80,235},
        .name = "测试完成",
        .type = widget_type_button,
        .userParam = RGB(216,216,216),
    },
};

static int Voice_flag = 0;
static int Uart_flag = 0;
static int Start_UartTest = 0;
void Set_TestUart_flag(int flag)
{
    Uart_flag = flag;
}

int Get_Start_UartTest()
{
    return Start_UartTest;
}
void Set_Start_UartTest(int statue)
{
    Start_UartTest = statue;
}

//按钮控件创建函数
static bool_t  BgeinTestButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
//    const char *course;
//    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);


        GDD_SetFillColor(hdc,buttoninfo->userParam);
        GDD_FillRect(hdc,&rc);

        GDD_SetTextColor(hdc,RGB(115,115,115));
        GDD_DrawText(hdc,buttoninfo->name,-1,&rc,DT_CENTER|DT_VCENTER);

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_BgeinTest(struct WindowMsg *pMsg)
{
//    RECT rc;
    HDC  hdc;
    HWND hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
//    HWND hwndButton;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, BgeinTestButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (BgeinTestCfgTab[i].type)
            {
                case  widget_type_button :
                    {
                        if(Voice_flag == 0 || i == BgeinTest_TextFinish)
                        {
                             Widget_CreateButton(BgeinTestCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                                     BgeinTestCfgTab[i].position.left, BgeinTestCfgTab[i].position.top,\
                                     GDD_RectW(&BgeinTestCfgTab[i].position),GDD_RectH(&BgeinTestCfgTab[i].position), //按钮位置和大小
                                     hwnd,i,(ptu32_t)&BgeinTestCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                        }
                    }
                    break;
                default:    break;
            }
        }
    }
    return true;
}

//绘制消息处函数
static bool_t HmiPaint_BgeinTest(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
//    const char * bmp ;
//    char* time;
//    RECT rc;

    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (BgeinTestCfgTab[i].type)
            {
                case  type_background :
                    GDD_SetFillColor(hdc,BgeinTestCfgTab[i].userParam);
                    GDD_FillRect(hdc,&BgeinTestCfgTab[i].position);
                    break;
                case  widget_type_text :
                    GDD_SetTextColor(hdc,BgeinTestCfgTab[i].userParam);
                    GDD_DrawText(hdc,BgeinTestCfgTab[i].name,-1,&BgeinTestCfgTab[i].position,DT_LEFT|DT_VCENTER);
                    if(i == BgeinTest_NetText)
                    {
                        if(Get_Wifi_Connectedflag()){
                            GDD_SetTextColor(hdc,RGB(0,255,0));
                            GDD_DrawText(hdc,"wifi连接测试成功",-1,&BgeinTestCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                        }else{
                            GDD_SetTextColor(hdc,RGB(255,0,0));
                            GDD_DrawText(hdc,"正在wifi连接，请稍等",-1,&BgeinTestCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                        }
                    }else if(i == BgeinTest_VoiceText){
                        if(Voice_flag)
                        {
                            GDD_SetTextColor(hdc,RGB(0,255,0));
                            GDD_DrawText(hdc,"喇叭测试成功",-1,&BgeinTestCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                        }
                    }else if(i == BgeinTest_UartText){
                        if(Uart_flag == 1)
                        {
                            GDD_SetTextColor(hdc,RGB(255,0,0));
                            GDD_DrawText(hdc,"采集数据失败",-1,&BgeinTestCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                        }else if(Uart_flag == 2)
                        {
                            GDD_SetTextColor(hdc,RGB(0,255,0));
                            GDD_DrawText(hdc,"采集模块测试成功",-1,&BgeinTestCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                        }else if(Voice_flag){
                            GDD_SetTextColor(hdc,BgeinTestCfgTab[i].userParam);
                            GDD_DrawText(hdc,"测试中，请稍等",-1,&BgeinTestCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                        }
                    }else if(i == BgeinTest_Hand){
                        if(!djy_gpio_read(GPIO8))
                        {
                            GDD_SetTextColor(hdc,RGB(255,0,0));
                            GDD_DrawText(hdc,"手势离开",-1,&BgeinTestCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                        }else{
                            GDD_SetTextColor(hdc,RGB(0,255,0));
                            GDD_DrawText(hdc,"手势放下",-1,&BgeinTestCfgTab[i].position,DT_RIGHT|DT_VCENTER);
                        }
                    }
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

//enum WinType Get_BgeinTestWinType();
enum WinType HmiNotify_BgeinTest(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);
    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case BgeinTest_Restart:
                VoicePrompt(Voice_Test);
                NextUi = WIN_BgeinTest;
                break;
            case BgeinTest_Confirm:
                Voice_flag = 1;
                Start_UartTest = 1;
                NextUi = WIN_BgeinTest;
                break;
            case BgeinTest_TextFinish:
                runapp(0);
                break;
            default:
                NextUi = WIN_BgeinTest;
                break;
        }
    }
    return NextUi;
}


int Register_BgeinTest()
{
    return Register_NewWin(WIN_BgeinTest,HmiCreate_BgeinTest,HmiPaint_BgeinTest,HmiNotify_BgeinTest);
}








