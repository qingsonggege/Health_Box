//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include <upgrade.h>
#include "project_config.h"
#include <app_flash.h>
//extern char start_user_loading;
//控件ID编号
enum ScreenTestId{
    ScreenTest_BACKGROUND, //背景
    ScreenTest_Button1,
    ScreenTest_Button2,
    ScreenTest_Button3,
    ScreenTest_Button4,

    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO ScreenTestCfgTab[ENUM_MAXNUM] =
{
    [ScreenTest_BACKGROUND] = {
        .position = {0,0,320,240},
        .name = "Background",
        .type = type_background,
        .userParam = RGB(255,0,0),
    },
    [ScreenTest_Button1] = {
        .position = {0,0,20,20},
        .name = "测试按钮1",
        .type = widget_type_button,
        .userParam = RGB(255,0,0),
    },
    [ScreenTest_Button2] = {
        .position = {0,220,20,240},
        .name = "测试按钮2",
        .type = widget_type_button,
        .userParam = RGB(0,0,255),
    },
    [ScreenTest_Button3] = {
        .position = {300,220,320,240},
        .name = "测试按钮3",
        .type = widget_type_button,
        .userParam = RGB(0,255,0),
    },
    [ScreenTest_Button4] = {
        .position = {300,0,320,20},
        .name = "测试按钮4",
        .type = widget_type_button,
        .userParam = RGB(255,255,255),
    },
};

static int button_flag = 1;
static int Test_Mode = 0;

extern  WIFI_CFG_T  LoadWifi;

int Get_Test_Mode()
{
    return Test_Mode;
}

//按钮控件创建函数
static bool_t  ScreenTestButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
//    const char *course;
//    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

//        if(0==strcmp(buttoninfo->name,ScreenTestCfgTab[ScreenTest_Button1].name))
//        {

            GDD_SetFillColor(hdc,buttoninfo->userParam);
            GDD_FillRect(hdc,&rc);

            GDD_SetDrawColor(hdc,RGB(115,115,115));
            GDD_DrawLine(hdc,rc.left,rc.top+10,rc.right,rc.top+10);
            GDD_DrawLine(hdc,rc.left+10,rc.top,rc.left+10,rc.bottom); //L

//        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_ScreenTest(struct WindowMsg *pMsg)
{
//    RECT rc;
    HDC  hdc;
    HWND hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
//    HWND hwndButton;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, ScreenTestButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    if(hdc)
    {
//        for(int i=0;i<ENUM_MAXNUM;i++)
//        {
//            switch (ScreenTestCfgTab[i].type)
//            {
//                case  widget_type_button :
                    if(ScreenTestCfgTab[button_flag].type == widget_type_button)
                    {
                     Widget_CreateButton(ScreenTestCfgTab[button_flag].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                             ScreenTestCfgTab[button_flag].position.left, ScreenTestCfgTab[button_flag].position.top,\
                             GDD_RectW(&ScreenTestCfgTab[button_flag].position),GDD_RectH(&ScreenTestCfgTab[button_flag].position), //按钮位置和大小
                             hwnd,button_flag,(ptu32_t)&ScreenTestCfgTab[button_flag], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                    }
//                    break;
//                default:    break;
//            }
//        }
    }
    return true;
}

//绘制消息处函数
static bool_t HmiPaint_ScreenTest(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
//    const char * bmp ;
//    char* time;
//    RECT rc;

    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (ScreenTestCfgTab[i].type)
            {
                case  type_background :
                    if(button_flag == 1)
                        GDD_SetFillColor(hdc,ScreenTestCfgTab[i].userParam);
                    else if(button_flag == 2)
                        GDD_SetFillColor(hdc,RGB(0,0,255));
                    else if(button_flag == 3)
                        GDD_SetFillColor(hdc,RGB(0,255,0));
                    else if(button_flag == 4)
                        GDD_SetFillColor(hdc,RGB(255,255,255));
                    GDD_FillRect(hdc,&ScreenTestCfgTab[i].position);
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
//enum WinType Get_ScreenTestWinType();
enum WinType HmiNotify_ScreenTest(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);
    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case ScreenTest_Button1:
                button_flag = 2;
                NextUi = WIN_ScreenTest;
                break;
            case ScreenTest_Button2:
                button_flag = 3;
                NextUi = WIN_ScreenTest;
                break;
            case ScreenTest_Button3:
                button_flag = 4;
                NextUi = WIN_ScreenTest;
                break;
            case ScreenTest_Button4:
                button_flag = 1;
                if(Get_Wifi_Connectedflag() == 0)
                {
                    strcpy(LoadWifi.WifiSsid,"DJYOS");
                    strcpy(LoadWifi.WifiPassWd,"1234567980");
                    Set_Start_ConnectWifi(1);
                }
                Test_Mode = 1;
                NextUi = WIN_BgeinTest;
                break;
            default:
                NextUi = WIN_ScreenTest;
                break;
        }
    }
    return NextUi;
}


int Register_ScreenTest()
{
    return Register_NewWin(WIN_ScreenTest,HmiCreate_ScreenTest,HmiPaint_ScreenTest,HmiNotify_ScreenTest);
}








