#include "mem_mgr.h"
#include <stdlib.h>

//typedef struct StMemMgr {
//    void *pmem;
//    int size;
//    void *pAudioBuf;
//    int SizeAudio;
//}StMemMgr;
//
//struct StMemMgr gMemMgr;
//
//struct HeapCB *my_heap=0;
//void *psram_malloc (unsigned int size)
//{
//
//    if (my_heap==0){
//        my_heap =Heap_FindHeap("PSRAM");
//        if(my_heap==NULL){
//            printf("M_FindHeapd  ERROR!\r\n");
//            return NULL;
//        }
//    }
//    return M_MallocHeap(size,my_heap,0);
//}
//
//void psram_free (void *p)
//{
//    return M_FreeHeap(p, my_heap);
//}
//
//#ifdef malloc
//#undef malloc
//#endif
//
//#ifdef free
//#undef free
//#endif
//
//#define malloc     psram_malloc
//#define free psram_free
//
//int MemMgrInit()
//{
//    struct StMemMgr *pMemMgr = &gMemMgr;
//
//    pMemMgr->pmem = psram_malloc (SHARE_BUF_SIZE);
//    if (pMemMgr->pmem == 0) {
//        printf("info: psram_malloc share buffer %d failed!\r\n", SHARE_BUF_SIZE);
//        return -1;
//    }
//    pMemMgr->size = SHARE_BUF_SIZE;
//
//    printf("info: psram_malloc %d successfully!\r\n", SHARE_BUF_SIZE);
//    return 0;
//}


#define AUDIO_BUF_SIZE (1024*1024)

void *GetUpdateBufAddr()
{
//  struct StMemMgr *pMemMgr = &gMemMgr;
//  if (pMemMgr->pmem == 0) return 0;
//  return pMemMgr->pmem;
    return malloc(4*1024*1024);
}

int GetAudioLoopBufSize()
{
//    struct StMemMgr *pMemMgr = &gMemMgr;
//    if (pMemMgr->pAudioBuf == 0) return 0;
//
//    return pMemMgr->SizeAudio;
    return AUDIO_BUF_SIZE;
}

void *GetAudioLoopBufAddr()
{
//    struct StMemMgr *pMemMgr = &gMemMgr;
//    if (pMemMgr->pAudioBuf == 0) return 0;
//
//    return pMemMgr->SizeAudio;
    return malloc(AUDIO_BUF_SIZE);
}
