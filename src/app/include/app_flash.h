#ifndef __APP_FLASH_H__
#define __APP_FLASH_H__

#include "cpu_peri.h"

#define FLASH_PAGE_SIZE 256
#define FLASH_SECTOR_SIZE 4096
//
//#define FLASH_START_ADDR 0x3E0000   //2M-100K
//
///* 从此地址开始用于数据的保存 */
//#define MACHINE_STRING_ADDR     (FLASH_START_ADDR)
//
///* WIFI 地址 */
//#define WIFI_INFO_ADDR           (MACHINE_STRING_ADDR + FLASH_SECTOR_SIZE)
///* USER 地址 */
//#define USER_MAC_ADDR           (WIFI_INFO_ADDR + FLASH_SECTOR_SIZE)
///* 授權地址 */
//#define AUTH_MAC_ADDR           (USER_MAC_ADDR + FLASH_SECTOR_SIZE)
///*国谷 NLP 登录信息保存地址*/
//#define NLP_INFO_ADDR           (AUTH_MAC_ADDR + FLASH_SECTOR_SIZE)
//
//#define SN_INFO_ADDR            (NLP_INFO_ADDR + FLASH_SECTOR_SIZE)
//
//#define DHCP_SPEC_IP            (SN_INFO_ADDR + FLASH_SECTOR_SIZE)
//
//#define TOUCH_RATIO_ADJUST      (DHCP_SPEC_IP + FLASH_SECTOR_SIZE)
//
//#define WIFI_MAC_ADDR           (TOUCH_RATIO_ADJUST + FLASH_SECTOR_SIZE)
//
//#define FLASH_FAST_DATA_ADDR        (WIFI_MAC_ADDR + FLASH_SECTOR_SIZE)

//#define CFG_WIFI_MAC_FILE_NAME        "/efs/wifi_mac.dat"
#define CFG_WIFI_FILE_NAME            "/efs/wifi.dat"
#define CFG_USER_WIFI_FILE_NAME       "/efs/user_wifi.dat"
#define CFG_SN_INFO_FILE_NAME         "/efs/sn_info.dat"
#define CFG_NLP_INFO_FILE_NAME        "/efs/nlp_info.dat"
#define CFG_DHCPIP_INFO_FILE_NAME     "/efs/dhcpip_info.dat"
#define CFG_DHCPIP_INFO_FILE_URL      "/efs/url_info.dat"
#define CFG_DEVICE_ID                 "/efs/devid_info.dat"
//#define CFG_FAST_DATA_FILE_NAME       "/efs/fast_data.dat"

typedef struct WifiCfg
{
    uint8 statue;
    char WifiSsid[32 + 1];        /* WIFI SSID *///加1是结束符
    char WifiPassWd[64 + 1];      /* WIFI 密码 *///加1是结束符
}WIFI_CFG_T;

typedef struct UrlCfg
{
    uint8 statue;
    char Weburl[32 + 1];        /*  *///加1是结束符
    int port;
}URL_CFG_T;

extern  char *NLP_SN_BUF;

bool_t Sn_InfoSave(char *info ,u32 len);
bool_t Sn_InfoLoad(char *buf ,u32 maxlen);
bool_t Sn_InfoErase( );

bool_t Nlp_InfoSave(char *info ,u32 len);
bool_t Nlp_InfoLoad(char *buf ,u32 maxlen);
bool_t Nlp_InfoErase( );

uint8 WifiLoad(void);
void WifiSave(char *ssid,char *key);
void WifiErase( );
uint8 UserLoad(void);
void UserSave(char *ssid,char *key);


bool_t DhcpIp_InfoLoad(unsigned int *ip);

bool_t DevidLoad(unsigned char *buf ,u32 maxlen);
bool_t DevidSave(unsigned char *devid ,u32 len);
#endif

