#ifndef __APP_WIFI_H__
#define __APP_WIFI_H__

#include "cpu_peri.h"

void Get_ScanRouter(void);
char *Get_RouteName(u32 para);
uint8 Get_RouteCnt(void);
int Get_RouteStrength(u32 para);
//int GetWifi_SaveStatue(char *ssid);
#endif
