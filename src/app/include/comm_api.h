/*
 * comm_api.h
 *
 *  Created on: 2019��11��23��
 *      Author: cc
 */

#ifndef APP_COMM_COMM_API_H_
#define APP_COMM_COMM_API_H_
//#include "other_api.h"
int OssMgrInit();
int OssOpen(char *bucket, char *path, int timeout_ms);
int OssWrite(char *data, int len, int timeout_ms);
int OssClose();
int  GetTimeStamp(unsigned int *timestamp_out, int timeout_ms);
int ClearTimeStamp();
int WebDownload(char *host, int port, char *path, void *fdo, int timeout_ms);




#endif /* APP_COMM_COMM_API_H_ */
