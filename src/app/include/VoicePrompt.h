/*
 * VoicePrompt.h
 *
 *  Created on: 2019年5月13日
 *      Author: c
 */

#ifndef APP_VOICEPROMPT_H_
#define APP_VOICEPROMPT_H_

#include <stddef.h>

typedef enum {
    wifi_connecting,        //连网中
    wifi_connect_sucess,    //网络连接成功
    wifi_connect_error,    //网络连接失败
    Voice_Test,
}Prompttone;

bool_t VoicePrompt(Prompttone cmd);

#endif /* APP_VOICEPROMPT_H_ */
