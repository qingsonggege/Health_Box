#include "stdint.h"
#include "stddef.h"
#include "stdlib.h"
#include <sys/socket.h>
#include <sys/dhcp.h>
#include <sys/dhcp.h>
#include <app_flash.h>
#include "wlan_ui_pub.h"
#include "project_config.h"
#include "board.h"
#include "djyos.h"
#include "int.h"
#include "cpu_peri_wifi.h"
#include <wifi_app.h>

char gip_addr[30] = {0};

extern  WIFI_CFG_T  LoadWifi;
//extern bool_t DhcpIp_InfoSave(char *info ,u32 len);

typedef struct RouterInfo {
    uint8 bssid[6]; /* MAC地址 */
    char ssid[32]; /* 路由名称 */
    char channel;  /* 信道 */
    int rssi;     /* 信号强度 */
    int security; /*0-open 1-wep 2-wpapsk 3-wpa2psk  */
}ROUTER_INFO_T;

typedef struct ywRouterScan
{
    uint8 ucRouterSum;
    ROUTER_INFO_T* tRouterInfo;
}ROUTER_SCAN_T;

ROUTER_SCAN_T tScanRouter = {
        0,
        NULL
};

char *DevGetIpAddr()
{
    return gip_addr;
}
#if 0
char *inet_ntoa(struct in_addr addr);

int cb_ip_get(u32 *ip)
{
    struct in_addr ipaddr;
    if (DhcpIp_InfoLoad(ip)) {
        ipaddr.s_addr = *ip;
        char *ip_str = inet_ntoa(ipaddr);
        if (ip_str && strcmp(ip_str,"0.0.0.0")!=0 && strcmp(ip_str,"255.255.255.255")!=0) {
            printf("info: dhcp ip address load from flash: %s !\r\n", ip_str);
            return 1;
        }
    }
    printf("info: dhcp ip address loaded from flash failed !\r\n");
    return 0;
}

int cb_ip_set(u32 ip)
{
    struct in_addr ipaddr;
    ipaddr.s_addr = ip;
    char *ip_str = inet_ntoa(ipaddr);
    u32 ip_temp;
    if (ip_str) {
        memset(gip_addr, 0, sizeof(gip_addr));
        if (ip_str && strcmp(ip_str,"0.0.0.0")!=0 && strcmp(ip_str,"255.255.255.255")!=0) {
            if (strlen(ip_str)<sizeof(gip_addr)) {
                strcpy(gip_addr, ip_str);
            }
            printf("info: Get IP Address: %s !\r\n", gip_addr);
        }
    }
    if (DhcpIp_InfoLoad(&ip_temp)) {
        if (ip_temp == ip) {
            printf("info: the same IP address!\r\n");
            return 0;
        }
    }
    printf("info: write flash the new IP address!\r\n");
    DhcpIp_InfoSave((char*)&ip,(u32) sizeof(ip));
    return 1;
}
#endif

static void scan_ApCallBack(void *arg, uint8_t vif_idx)
{
    UNUSED(arg);
    UNUSED(vif_idx);
//    u32 buflen = sizeof(wifiinfobuf);
    struct sta_scan_res *scan_result = 0;
//    char apbuf[50];
//    u32 len;
    uint32_t i,j;
    uint32_t num = 0;
    uint32_t cnt ;
    int connect_level = -40;
//    LinkStatusTypeDef outStatus;

    cnt = DjyWifi_GetScanResult(&scan_result);
    //printf("scan_ApCallBack  cnt = %d\r\n",cnt);

    Set_flag_ScamWifi(0);
#if 1
    if(Get_Wifi_Connectedflag())
    {
        cnt = cnt+1;
        for(i=0;i<(cnt-1);i++)
        {
            if(0 == strcmp(scan_result[i].ssid,LoadWifi.WifiSsid))
            {
                memset(scan_result[i].ssid,0,sizeof(scan_result[i].ssid));
                connect_level = scan_result[i].level;
                printf("connect_level");
//                break;
            }
        }
    }else{
        for(i=0;i<cnt;i++)
        {
            if(0 == strcmp(scan_result[i].ssid,LoadWifi.WifiSsid))
            {
                Set_Start_ConnectWifi(2);
                break;
            }
        }
    }
    if(NULL == scan_result)
        goto error;

    if(NULL != tScanRouter.tRouterInfo)
    {
        tScanRouter.ucRouterSum = 0;
        free(tScanRouter.tRouterInfo);
        tScanRouter.tRouterInfo = NULL;
    }

    tScanRouter.tRouterInfo = malloc(sizeof(ROUTER_INFO_T) * cnt);

    if(Get_Wifi_Connectedflag())
    {
        cnt = cnt -1;
        strcpy(tScanRouter.tRouterInfo[num].ssid,LoadWifi.WifiSsid);
        tScanRouter.tRouterInfo[num].rssi= connect_level;//outStatus.wifi_strength;
        num ++;
    }
    for(i=0;i<cnt;i++)
    {
        if(strlen(scan_result[i].ssid) == 0 || scan_result[i].level < -77)
            continue;
        for(j=0;j<i;j++)//去掉重名
        {
            if(0 == strcmp(scan_result[i].ssid,scan_result[j].ssid))
                break;
        }
        if(i!=j)
            continue;

        memcpy(tScanRouter.tRouterInfo[num].bssid,scan_result[i].bssid, 6);
        strcpy(tScanRouter.tRouterInfo[num].ssid,scan_result[i].ssid);
        tScanRouter.tRouterInfo[num].channel = scan_result[i].channel;
        tScanRouter.tRouterInfo[num].rssi    = scan_result[i].level;
        tScanRouter.tRouterInfo[num].security= scan_result[i].security;
//        strcpy(apbuf,scan_result[i].ssid);
        //printf("scan_result[%d]=%s Level=%d \r\n",num,tScanRouter.tRouterInfo[num].ssid ,tScanRouter.tRouterInfo[num].rssi);
        num ++;
    }
    tScanRouter.ucRouterSum = num;
    Update_RouterInfo();
#endif
error:
    if(scan_result) free(scan_result);
}

void Get_ScanRouter(void)
{
    printf("--Get_ScanRouter--\r\n");
    if (Get_Wifi_Connectedflag()) { //连接成功，这时可以扫描网络
        printf("--Get_Wifi_Connectedflag == 1 --\r\n");
    }
    else {//连接不成功，这时不可以扫描网络，必须先断开。
        DjyWifi_StaDisConnect();
        printf("--Get_Wifi_Connectedflag == 0 && DjyWifi_StaDisConnect--\r\n");
    }
//    DJY_EventDelay(5000*1000);
    DjyWifi_StartScan(scan_ApCallBack);
}

char *Get_RouteName(u32 para)
{
    //printf("para = %d\r\n",para);
    return tScanRouter.tRouterInfo[para].ssid;
}

uint8 Get_RouteCnt(void)
{
    return tScanRouter.ucRouterSum;
}

int Get_RouteStrength(u32 para)
{
    int ret;
    if(tScanRouter.tRouterInfo[para].rssi < -88){
        ret = 1;
    }else if(tScanRouter.tRouterInfo[para].rssi < -77){
        ret = 2;
    }else if(tScanRouter.tRouterInfo[para].rssi < -50){
        ret = 3;
    }else{
        ret =4 ;
    }
    return ret;
}

int GetWifi_SaveStatue(char *ssid)
{
    int ret=0;
#if(CN_BEKEN_SDK_V3 == 1)
#include "role_launch.h"
    RL_BSSID_INFO_T ap_info;
#else
    struct wlan_fast_connect ap_info;
#endif
    ret = wlan_fast_info_match(ssid,0,&ap_info);

    return ret;
}
