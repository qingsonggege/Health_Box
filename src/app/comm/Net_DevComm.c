//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * Net_devComm.c
 *
 *  Created on: 2020年1月3日
 *      Author: czz
 */

#include "Net_DevComm.h"

#include <djyos.h>
#include <mongoose.h>
#include "stdlib.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <systime.h>
#include <stdlib.h>
#include <cJSON.h>
#include "app_flash.h"

#ifndef free
#define free(x) M_Free(x)
#endif

typedef struct StWorkData {
    int status;
    char *new_data;
    int new_len;
}StWorkData;

extern URL_CFG_T  LoadUrl;

int DoHttpBody(char *body, int len, char *outbuf, int outlen)
{
    int ret = 1;
    cJSON *cjson = 0;
    if (body == 0 || len <= 0) return -1;
    char * pJsonStr = 0;
    cjson = cJSON_Parse(body);
    if (cjson)
    {
        cJSON*  results = cJSON_GetObjectItem(cjson, "result");
        if (results)
        {
            printf("submit success: %s\r\n", results->valuestring);
            if (strcmp(results->valuestring, "SUCCESS") == 0)
            {
                cJSON* subjson = cJSON_GetObjectItem(cjson, "data");
                if (subjson)
                {
                    pJsonStr = cJSON_PrintUnformatted(subjson);
                    if(pJsonStr == NULL)
                    {
                        ret = -2;
                    }else if((int)strlen(pJsonStr) + 1 < outlen )
                    {
                        strcpy(outbuf, pJsonStr);
                        printf("message: %s\r\n", pJsonStr);
                    }else
                    {
                        printf("outbuf overflow error");
                        ret = -3;
                    }
                }
            }
//            else{
//                printf("result invalid\r\n");
//                ret = -4;
//            }
        }
    }

//END_FUN:
    if (cjson) cJSON_Delete(cjson);
//    if (pnew) free(pnew);
    if (pJsonStr) free(pJsonStr);
    return ret;
}

static void cb_ev_handler(struct mg_connection *nc, int ev, void *ev_data) {
    struct http_message *hm = (struct http_message *) ev_data;
    struct StWorkData *pQuestData = (struct StWorkData*)nc->user_data;
    char *p = 0;

    int is_chunked = 0;
    struct mg_str *s;
    if (hm) {
        if ((s = mg_get_http_header(hm, "Transfer-Encoding")) != NULL &&
            mg_vcasecmp(s, "chunked") == 0) {
            is_chunked = 1;
        }
    }
    switch (ev) {
    case MG_EV_CONNECT:
        if (*(int *)ev_data != 0) {
            /*  fprintf(stderr, "connect() failed: %s\n", strerror(*(int *)ev_data));*/
            nc->flags |= MG_F_CLOSE_IMMEDIATELY;
            pQuestData->status = -1;
        }
        int opt = 1024*12;
        if(0 != setsockopt(nc->sock, SOL_SOCKET ,SO_SNDBUF,&opt, 4))
        {
            printf("error: cb_http_upload_handler set client sndbuf failed!\r\n");
            nc->flags |= MG_F_CLOSE_IMMEDIATELY;
            pQuestData->status = -1;
        }
        
        if (pQuestData->new_data) {
            free(pQuestData->new_data);
        }
        pQuestData->new_data = 0;
        pQuestData->new_len = 0;
        break;
    case MG_EV_HTTP_REPLY:
    {
        switch (hm->resp_code) {
        case 200:
            nc->flags |= MG_F_CLOSE_IMMEDIATELY;
            if(GetRtcTickSecTimestamp() == 0)
            {
                for (int i = 0; i<10; i++) {
                    char *pname = malloc(hm->header_names[i].len+1);
                    char *pval = malloc(hm->header_values[i].len+1);
                    if (pname){
                        memset(pname, 0, hm->header_names[i].len+1);
                        memcpy(pname, hm->header_names[i].p, hm->header_names[i].len);
                        printf("name = %s\r\n", pname);
                    }
                    if (pval){
                        memset(pval, 0, hm->header_values[i].len+1);
                        memcpy(pval, hm->header_values[i].p, hm->header_values[i].len);
                    }
                    if(0==strcmp(pname,"Date"))//返回
                    {
                        Set_login_ok(1);
                        synDateWithHttpPkg(pval);
                        if (pname) free(pname);
                        if (pval) free(pval);
                        break;
                    }
                    if (pname) free(pname);
                    if (pval) free(pval);
                }
            }
            if (hm->body.len <= 0) {
                if (pQuestData->status == 0) pQuestData->status = -2;
                break;
            }

            //process the whole body.
            pQuestData->status = -3;
            p = realloc(pQuestData->new_data, hm->body.len+1);
            if (p == 0) {
                printf("error: cb_ev_handler->realloc MG_EV_HTTP_REPLY, p==null!\r\n");
                break;
            }
            pQuestData->new_data = p;
            pQuestData->new_len = hm->body.len+1;
            memcpy(pQuestData->new_data, hm->body.p, hm->body.len);
            pQuestData->new_data[hm->body.len] = 0;

            if (DoHttpBody(pQuestData->new_data, pQuestData->new_len, pQuestData->new_data, pQuestData->new_len) > 0) {
                pQuestData->status = 1;
            }
            break;
        case 302:
            pQuestData->status = -302;
            break;
        case 401:
            pQuestData->status = -401;
            break;
        case 404:
            pQuestData->status = -404;
            break;
        default:
            pQuestData->status = -5;
            break;
        }
        break;
    }

    case MG_EV_HTTP_CHUNK:
    {
        if (!is_chunked) break;

        nc->flags = MG_F_DELETE_CHUNK;
        //hex_dump("cb_http_upload_handler MG_EV_HTTP_CHUNK:", hm->body.p, hm->body.len);
        if (hm->body.len > 0) {
            p = realloc(pQuestData->new_data, pQuestData->new_len + hm->body.len);
            if (p == 0) {
                printf("error: cb_ev_handler->realloc MG_EV_HTTP_CHUNK, p==null!\r\n");
                break;
            }
            pQuestData->new_data = p;
            memcpy(&pQuestData->new_data[pQuestData->new_len], hm->body.p, hm->body.len);
            pQuestData->new_len += hm->body.len;
        }
        else if (hm->body.len == 0) {//end flag
            if (pQuestData->status != 0) break;
            p = realloc(pQuestData->new_data, pQuestData->new_len+1);
            if (p == 0) {
                printf("error: cb_ev_handler->realloc MG_EV_HTTP_CHUNK, p==null!\r\n");
                break;
            }
            pQuestData->new_data = p;
            pQuestData->new_data[pQuestData->new_len] = 0;
            pQuestData->new_len += 1;

            //process the whole body.
            pQuestData->status = -6;

            if (DoHttpBody(pQuestData->new_data, pQuestData->new_len, pQuestData->new_data, pQuestData->new_len) > 0) {
                printf("%s\r\n", pQuestData->new_data);
                pQuestData->status = 1;
            }
        }
        break;
    }

    case MG_EV_CLOSE:
            if (pQuestData && pQuestData->status==0) {
                pQuestData->status = -7;
            }
        break;
    default:
        break;
    }
}

int DevGetCommon(NetAccess *Nacb)
{
    struct StWorkData user_data;
    memset(&user_data, 0, sizeof(user_data));
    int ret = 0;
    char *meta = NULL;
    char *url = NULL;
    struct mg_connection *nc;
    struct mg_mgr mgr;

    mg_mgr_init(&mgr, NULL);

    size_t len = strlen(Nacb->path)+strlen(Nacb->net_host)+strlen("http://")+1;

    url = (char*)malloc(len);
    if (url == 0) return -1;

    meta = (char*)malloc(1024);
    if (meta == 0) {
        ret = -1;
        goto END_QUEST;
    }
    user_data.new_len = 0;
    user_data.new_data = 0;

    if(Nacb->Type != NULL)
    {
        sprintf(meta,
            "Accept: */*\r\n"
            "Connection: keep-alive\r\n"
            "User-Agent: Mongoose/6.15\r\n"
            "Cache-Control: no-cache\r\n"
            "Content-Type: application/%s\r\n",Nacb->Type);
    }else
    {
        ret = -3;
        goto END_QUEST;
    }
//    else
//    {
//
//        char *httpbodayhead =  "Accept: */*\r\n"
//                                "Connection: keep-alive\r\n"
//                                "User-Agent: Mongoose/6.15\r\n"
//                                "Cache-Control: no-cache\r\n"
//                                "Content-Type: multipart/form-data; boundary=%s\r\n";
//        if(strlen( Nacb->boundary)+strlen(httpbodayhead) >= 1024)
//        {
//            ret = -3;
//            goto END_QUEST;
//        }
//        sprintf(meta,httpbodayhead, Nacb->boundary);
//    }

    sprintf(url, "http://%s%s", Nacb->net_host, Nacb->path);
    nc = mg_connect_http(&mgr, cb_ev_handler, url, meta, Nacb->post_data);
    if (nc == 0) goto END_QUEST;
    nc->user_data = &user_data;

    unsigned int timemark = DJY_GetSysTime()/1000;
    unsigned int timeout_ms = 10*1000;
    while (user_data.status == 0) {
//        printf("user_data.status = %d\r\n",user_data.status);
        if ((DJY_GetSysTime()/1000-timemark) > timeout_ms) {
            printf("error: DevGetCommon: timeout break!\r\n");
            break;
        }
        mg_mgr_poll(&mgr, 500);
        DJY_EventDelay(10*1000);
    }
    if (user_data.status == 1) {
        if (user_data.new_data) {
            int min = strlen(user_data.new_data) + 1;
            min = min < Nacb->buflen ? min : Nacb->buflen;
            memcpy(Nacb->out_json, user_data.new_data, min);
            user_data.status = min;
        }
        else {
            user_data.status = 0;
        }
    }
    ret = user_data.status;
END_QUEST:
    mg_mgr_free(&mgr);
    if (meta) free(meta);
    if (url) free(url);
    if (user_data.new_data) {
        free(user_data.new_data);
        user_data.new_data = 0;
        user_data.new_len = 0;
    }
    printf("DevGetCommon = %d\r\n",ret);
    return ret;
}

int DevGetCommonXXX(NetAccess *Nacb)
{
    struct StWorkData user_data;
       memset(&user_data, 0, sizeof(user_data));
       int ret = 0;
//       char *meta = NULL;
//       char *url = NULL;
       struct mg_connection *nc;
       struct mg_mgr mgr;
       char *temp = 0;
       int len = 0;
       mg_mgr_init(&mgr, NULL);

       user_data.new_len = 0;
       user_data.new_data = 0;

       if (Nacb->Type == NULL)
       {
           ret = -3;
           goto END_QUEST;
       }

       printf("------------xxxxxxxxxxxx start = %d --------------------\r\n",  (int)(DJY_GetSysTime()/1000));
//       sprintf(url, "http://%s%s", Nacb->net_host, Nacb->path);

       temp = (char*)malloc(1024);
       memset(temp, 0, 1024);

       sprintf(temp, "%s:%d", Nacb->net_host, LoadUrl.port);
#if 0
       sprintf(temp, "%s:%d", Nacb->net_host, 8188);
//#else
       sprintf(temp, "%s:%d", Nacb->net_host, 80);
#endif
       nc = mg_connect(&mgr, temp, cb_ev_handler);

       if (nc == 0)
       {
          ret = -6;
          goto END_QUEST;
      }

      char *pDomain = inet_ntoa(nc->sa.sin.sin_addr);
      if (pDomain==0)
      {
          ret = -7;
          goto END_QUEST;
      }

       nc->user_data = &user_data;
       mg_set_protocol_http_websocket(nc);

       len = 0;
       if (Nacb->post_data) {
           len = strlen(Nacb->post_data);
           printf("==============len=%d===========\r\n", len);
       }

       memset(temp, 0, 1024);
       sprintf(temp,
           "POST %s HTTP/1.1\r\n"
           "Host: %s\r\n"
           "User-Agent: Mongoose/6.15\r\n"
           "Content-Length: %d\r\n"
           "Connection: keep-alive\r\n"
           "Cache-Control: no-cache\r\n"
           "Content-Type: application/%s\r\n\r\n", Nacb->path,pDomain, len, Nacb->Type);
       mg_printf(nc, "%s", temp);

       size_t mark_send_len = 0; //用来记录超时前发送数据是否被移动，如果有移动，重置超时
       int mark = 0;
       int step = 10 * 1024;

       unsigned int timemark = DJY_GetSysTime() / 1000;
       unsigned int timeout_ms = 60*1000;
       while (user_data.status == 0) {
           if (DJY_GetSysTime() / 1000 - timemark > timeout_ms) {
               printf("error: DevGetCommon: timeout break!\r\n");
               break;
           }
           if (mark == len) {
               //继续等待，等待接收包或超时才真正结束
           }
           else if (mark + step < len) {
               mg_send(nc, &Nacb->post_data[mark], step);
               mark += step;
           }
           else {
               mg_send(nc, &Nacb->post_data[mark], len - mark);
               mark = len;
           }
           mark_send_len = nc->send_mbuf.len;
           while (nc->send_mbuf.len > 1024 && user_data.status == 0)
           {
               if (nc->send_mbuf.len < mark_send_len) { //数据有发送出去，就重置超时
                   timemark = DJY_GetSysTime() / 1000;
                   mark_send_len = nc->send_mbuf.len;
               }
               mg_mgr_poll(&mgr, 500);
               DJY_EventDelay(10*1000);
               if (DJY_GetSysTime() / 1000 - timemark > timeout_ms) {
                   printf("error: DevGetCommon: timeout break!\r\n");
                   break;
               }
           }

           mg_mgr_poll(&mgr, 500);
           //DJY_EventDelay(10*1000);
       }

       if (user_data.status == 1) {

           if (user_data.new_data) {
               int min = strlen(user_data.new_data) + 1;
               min = min < Nacb->buflen ? min : Nacb->buflen;
               memcpy(Nacb->out_json, user_data.new_data, min);
               user_data.status = min;
           }
           else {
               user_data.status = 0;
           }
       }
       ret = user_data.status;

   END_QUEST:
       mg_mgr_free(&mgr);
       if (temp) free(temp);
//       if (url) free(url);
       if (user_data.new_data) {
           free(user_data.new_data);
           user_data.new_data = 0;
           user_data.new_len = 0;
       }
       return ret;
}

#if 0
const char AAA[] = {
#include "ABC.h"
};

void testAAA()
{
    NetAccess Nacb;
    Nacb.path = "/user-app/healthCabin/sendResult";
    Nacb.net_host = "47.97.254.199";
    Nacb.Type = "x-www-form-urlencoded";
    Nacb.post_data = AAA;
    Nacb.out_json = malloc(2048);
    Nacb.buflen = 2048;

    DevGetCommonXXX(&Nacb);
}
#include "shell.h"
bool_t init_jtestAAA(char *param)
{
    (void)param;
    testAAA();
    return true;
}

ADD_TO_ROUTINE_SHELL(testaa,init_jtestAAA,"aaa");
#endif
