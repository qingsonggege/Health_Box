/****************************************************
 *  Automatically-generated file. Do not edit!	*
 ****************************************************/

#ifndef __PROJECT_CONFFIG_H__
#define __PROJECT_CONFFIG_H__

#include <stdint.h>
#include <stddef.h>
//manual config start
//此处填写手动配置，DIDE生成配置文件时，不会被修改
//manual config end

#define    CN_RUNMODE_IBOOT                0                //IBOOT模式运行
#define    CN_RUNMODE_APP                  2                //由IBOOT加载的APP
#define    CFG_RUNMODE                     CN_RUNMODE_APP           //由IBOOT加载的APP
#define    CN_RUNMODE_BOOTSELF             1                       //无须IBOOT，自启动模式APP
//*******************************  Configure black box  ******************************************//
#define    CFG_MODULE_ENABLE_BLACK_BOX     true
//*******************************  Configure device file system  ******************************************//
#define CFG_DEVFILE_LIMIT       10                  // "设备数量",定义设备数量
#define    CFG_MODULE_ENABLE_DEVICE_FILE_SYSTEM  true
//*******************************  Configure file system  ******************************************//
#define CFG_CLIB_BUFFERSIZE            512                  // "C库文件用户态缓冲区尺寸"
#define    CFG_MODULE_ENABLE_FILE_SYSTEM   true
//*******************************  Configure int  ******************************************//
#define    CFG_MODULE_ENABLE_INT           true
//*******************************  Configure kernel object system  ******************************************//
#define CFG_OBJECT_LIMIT        8               // "对象数初始值"，用完会自动扩充
#define CFG_HANDLE_LIMIT        8               // "句柄数初始值"，用完会自动扩充
#define    CFG_MODULE_ENABLE_KERNEL_OBJECT_SYSTEM  true
//*******************************  Configure lock  ******************************************//
#define CFG_LOCK_LIMIT          40                  // "锁的数量",定义锁的数量，包含信号量和互斥量
#define    CFG_MODULE_ENABLE_LOCK          true
//*******************************  Configure memory pool  ******************************************//
#define CFG_MEMPOOL_LIMIT       20                            // "内存池数量限值",
#define    CFG_MODULE_ENABLE_MEMORY_POOL   true
//*******************************  Configure message queue  ******************************************//
#define    CFG_MODULE_ENABLE_MESSAGE_QUEUE true
//*******************************  Configure multiplex  ******************************************//
#define    CFG_MODULE_ENABLE_MULTIPLEX     true
//*******************************  Configure ring buffer and line buffer  ******************************************//
#define    CFG_MODULE_ENABLE_RING_BUFFER_AND_LINE_BUFFER  true
//*******************************  Configure watch dog  ******************************************//
#define CFG_WDT_LIMIT           10                  // "看门狗数量",允许养狗数量
#define    CFG_MODULE_ENABLE_WATCH_DOG     true
//*******************************  Configure xip iboot file system  ******************************************//
#define    CFG_MODULE_ENABLE_XIP_IBOOT_FILE_SYSTEM  true
//*******************************  Configure fat file system  ******************************************//
#define CFG_FAT_MS_INSTALLUSE       MS_INSTALLUSE    //  "选项",参考filesystem.h中的MS_INSTALLUSE等定义
#define CFG_FAT_MEDIA_KIND          "MSC"       //  "媒体所属类别",（如"RAM","NAND","CF","SD", "MSC", "EMMC"）
#define CFG_FAT_MOUNT_POINT         "fat"       //  "安装路径",FAT文件系统安装目录
#define    CFG_MODULE_ENABLE_FAT_FILE_SYSTEM  true
//*******************************  Configure cpu onchip gpio  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_GPIO  true
//*******************************  Configure board config  ******************************************//
#define    CFG_MODULE_ENABLE_BOARD_CONFIG  true
//*******************************  Configure sdcard power  ******************************************//
#define    CFG_MODULE_ENABLE_SDCARD_POWER  true
//*******************************  Configure cpu onchip lowpower control  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_LOWPOWER_CONTROL  true
//*******************************  Configure cpu onchip random  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_RANDOM  true
//*******************************  Configure djybus  ******************************************//
#define    CFG_MODULE_ENABLE_DJYBUS        true
//*******************************  Configure spi bus  ******************************************//
#define    CFG_MODULE_ENABLE_SPI_BUS       true
//*******************************  Configure iicbus  ******************************************//
#define    CFG_MODULE_ENABLE_IICBUS        true
//*******************************  Configure io analog iic bus  ******************************************//
#define    CFG_MODULE_ENABLE_IO_ANALOG_IIC_BUS  true
//*******************************  Configure ioiicconfig  ******************************************//
#define CFG_IO_IIC_BUS_NAME   "IoIic"                               //  "给总线命名"
#define    CFG_MODULE_ENABLE_IOIICCONFIG   true
//*******************************  Configure easy file system  ******************************************//
#define CFG_EFS_FILE_SIZE_LIMIT           3840                                             // "单个文件大小的上限"
#define CFG_EFS_MAX_CREATE_FILE_NUM       14                                               // "允许创建最大文件数"
#define CFG_EFS_MAX_OPEN_FILE_NUM         10                                               // "允许同时打开最大文件数"
#define CFG_EFS_MOUNT_POINT               "efs"                                  // "EFS文件系统安装目录"
#define CFG_EFS_INSTALL_OPTION            MS_INSTALLCREAT                                  // "安装选项"，MS_INSTALLFORMAT,MS_INSTALLCREAT,MS_INSTALLUSE中一个或多个的“或”
#define    CFG_MODULE_ENABLE_EASY_FILE_SYSTEM  true
//*******************************  Configure font  ******************************************//
#define CFG_FONT_DEFAULT  "gb2312_song_16"                  // "默认字体",字体名在include/font目录中找
#define    CFG_MODULE_ENABLE_FONT          true
//*******************************  Configure kernel  ******************************************//
#define CFG_INIT_STACK_SIZE     4096                                // "初始化栈空间",定义初始化过程使用的栈空间，一般按默认值就可以了
#define CFG_EVENT_LIMIT         30                                  // "事件数量限值",事件数量
#define CFG_EVENT_TYPE_LIMIT    30                                  // "事件类型数限值",事件类型数
#define CFG_MAINSTACK_LIMIT     4096                                // "main函数栈尺寸",main函数运行所需的栈尺寸
#define CFG_IDLESTACK_LIMIT     2048                                // "IDLE事件栈尺寸",IDLE事件处理函数运行的栈尺寸，一般按默认值就可以了
#define CFG_IDLE_MONITOR_CYCLE  30                                  // "IDLE监视周期",监视IDLE事件持续低于1/16 CPU占比的时间，秒数，0=不监视
#define CFG_OS_TINY             false                               // "tiny版内核",true=用于资源紧缺的场合，内核会裁剪掉：事件类型名字，事件处理时间统计。
#define    CFG_MODULE_ENABLE_KERNEL        true
//*******************************  Configure misc  ******************************************//
#define    CFG_MODULE_ENABLE_MISC          true
//*******************************  Configure Nls Charset  ******************************************//
#define CFG_LOCAL_CHARSET       "gb2312"                    // "本地字符集",
#define    CFG_MODULE_ENABLE_NLS_CHARSET   true
//*******************************  Configure gb2312 charset  ******************************************//
#define    CFG_MODULE_ENABLE_GB2312_CHARSET  true
//*******************************  Configure gb2312 dot  ******************************************//
#define CFG_GB2312_12_SONG              zk_disable                          // "12点阵宋体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_16_SONG              from_array                          // "16点阵宋体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_16_YAHEI             zk_disable                          // "16点阵微软雅黑",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_16_YUAN              zk_disable                          // "16点阵圆体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_16_KAI              zk_disable                           // "16点阵楷体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_16_HEI              zk_disable                           // "16点阵黑",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_16_FANG              zk_disable                          // "16点阵仿宋体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_24_SONG              zk_disable                          // "24点阵宋体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_24_YAHEI              zk_disable                         //  "24点阵微软雅黑",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              //  "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_24_YUAN              zk_disable                          // "24点阵圆体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_24_KAI              zk_disable                           // "24点阵楷体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_24_HEI              zk_disable                           // "24点阵黑体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_24_FANG              zk_disable                          // "24点阵仿宋体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_32_SONG              zk_disable                          // "32点阵宋体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_32_YUAN              zk_disable                          // "32点阵圆体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_32_KAI              zk_disable                           // "32点阵楷体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_32_HEI              zk_disable                           // "32点阵黑体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_32_FANG              zk_disable                          // "32点阵仿宋体",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"              // "字库文件名",若从文件读取，则配置文件名
#define    CFG_MODULE_ENABLE_GB2312_DOT    true
//*******************************  Configure time  ******************************************//
#define CFG_LOCAL_TIMEZONE      8                  // "时区",北京时间是东8区
#define    CFG_MODULE_ENABLE_TIME          true
//*******************************  Configure utf8 charset  ******************************************//
#define    CFG_MODULE_ENABLE_UTF8_CHARSET  true
//*******************************  Configure cpu onchip audio  ******************************************//
#define CFG_RX_DMA_BUF_SIZE     8192                    // "采样DMA buffer size",
#define CFG_AUDIO_SAMPLE_RATE   8000                // "默认音频采样率",
#define CFG_USE_AUD_DAC         true            // 
#define CFG_USE_AUD_ADC         true            // 
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_AUDIO  true
//*******************************  Configure cpu onchip boot  ******************************************//
#define CFG_POWER_ON_RESET_TO_BOOT    true                             // 控制上电复位后是否强制运行iboot
#define    CFG_MODULE_ENABLE_BOOT          true
//*******************************  Configure cpu onchip flash  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_FLASH  true
//*******************************  Configure embflash install efs  ******************************************//
#define    CFG_EMBFLASH_EFS_MOUNT_NAME     "efs"                                         // "文件系统mount点名字",需要挂载的efs文件系统mount点名字
#define    CFG_EMBFLASH_EFS_PART_START     1008                                          // "分区起始"，填写块号，块号从0开始计算
#define    CFG_EMBFLASH_EFS_PART_END       1024                                          // "分区结束"，-1是最后一块
#define    CFG_EMBFLASH_EFS_PART_FORMAT    false                                         // "分区选项",是否需要格式化该分区。
#define    CFG_MODULE_ENABLE_EMBFLASH_INSTALL_EFS  true
//*******************************  Configure emflash insatall xip  ******************************************//
#define    CFG_EFLASH_XIP_PART_START       0                                             // "分区起始块号，含"，填写块号，块号从0开始计算
#define    CFG_EFLASH_XIP_PART_END         80                                            // "分区结束块号，不含"，-1表示最后一块，如果结束块是6，起始块是0，则该分区使用的块为0，1，2，3，4，5块
#define    CFG_EFLASH_XIP_PART_FORMAT      false                                         // "分区选项",是否需要格式化该分区。
#define    CFG_EFLASH_XIPFSMOUNT_NAME      "xip-iboot"                                   // "文件系统的mount位置"
#define    CFG_MODULE_ENABLE_EMFLASH_INSATALL_XIP  true
//*******************************  Configure cpu onchip pwm  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_PWM  true
//*******************************  Configure debug information  ******************************************//
#define    CFG_MODULE_ENABLE_DEBUG_INFORMATION  true
//*******************************  Configure flash  ******************************************//
#define    CFG_MODULE_ENABLE_FLASH         true
//*******************************  Configure graphical kernel  ******************************************//
#define CFG_GKERNEL_CMD_DEEP        1024                // "命令缓冲区长度",上层应用（例如gdd）向gkernel传递命令的缓冲区长度（字节数）
#define CFG_USER_REQUEST_DEEP       128                 // "请求缓冲区长度",gkernel向上层请求命令的缓冲区长度（字节数）
#define    CFG_MODULE_ENABLE_GRAPHICAL_KERNEL  true
//*******************************  Configure heap  ******************************************//
#define CFG_DYNAMIC_MEM true              // "全功能动态分配",即使选false，也允许使用malloc-free分配内存，但使用有差别，详见 《……用户手册》内存分配章节
#define    CFG_MODULE_ENABLE_HEAP          true
//*******************************  Configure cpu onchip qspi  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_QSPI  true
//*******************************  Configure uart device file  ******************************************//
#define    CFG_MODULE_ENABLE_UART_DEVICE_FILE  true
//*******************************  Configure cpu onchip uart  ******************************************//
#define    CFG_UART1_SENDBUF_LEN           256               //
#define    CFG_UART1_RECVBUF_LEN           2048              //
#define    CFG_UART2_SENDBUF_LEN           256               //
#define    CFG_UART2_RECVBUF_LEN           256               //
#define    CFG_UART1_ENABLE                true              //
#define    CFG_UART2_ENABLE                true              //
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_UART  true
//*******************************  Configure tcpip  ******************************************//
#define     CFG_NETPKG_MEMSIZE          0x6000                              // "数据包缓冲区尺寸"
#define    CFG_MODULE_ENABLE_TCPIP         true
//*******************************  Configure sock  ******************************************//
#define     CFG_SOCKET_NUM              10                  // "socket数限值"，占一个 tagItem 结构
#define    CFG_MODULE_ENABLE_SOCK          true
//*******************************  Configure ppp  ******************************************//
#define    CFG_MODULE_ENABLE_PPP           true
//*******************************  Configure dhcp  ******************************************//
#define     CFG_DHCPD_ENABLE            false               // "DHCP 服务器使能"
#define     CFG_DHCPC_ENABLE            true                // "DHCP 客户端使能"
#define     CFG_DHCP_RENEWTIME          3600                // "renew timer",秒数
#define     CFG_DHCP_REBINDTIME         3600                // "rebind timer",秒数
#define     CFG_DHCP_LEASETIME          3600                // "lease timer",秒数
#define     CFG_DHCPD_IPNUM             0x40                // "IP池尺寸",64
#define     CFG_DHCPD_IPSTART           "192.168.0.2"                // "DHCP起始IP",
#define     CFG_DHCPD_SERVERIP          "192.168.0.253"              // "DHCP SERVER IP"
#define     CFG_DHCPD_ROUTERIP          "192.168.0.253"              // "DHCP ROUTER SERVER IP"
#define     CFG_DHCPD_NETIP             "255.255.255.0"              // "DHCP MASK IP"
#define     CFG_DHCPD_DNS               "192.168.0.253"              // "DHCP DNSSERVER IP"
#define     CFG_DHCPD_DOMAINNAME       "domain"                    // "DHCP domain name"
#define    CFG_MODULE_ENABLE_DHCP          true
//*******************************  Configure arp  ******************************************//
#define     CFG_ARP_HASHLEN             32                  // "ARP哈希表长度"，占用一个指针
#define    CFG_MODULE_ENABLE_ARP           true
//*******************************  Configure cpu onchip MAC  ******************************************//
#define CFG_WIFI_DEV_NAME        "BK7251_WiFi"            // "网卡名称",
#define CFG_FAST_DATA_FILE_NAME "/efs/fast_data.dat"                // "存快联信息的文件路径"
#define CFG_MAC_DATA_FILE_NAME "/efs/mac.dat"                // "存MAC地址的文件路径"
#define CFG_AP_DHCPD_IPV4      "192.168.0.253"                 // "AP模式主机IP"
#define CFG_AP_DHCPD_SUBMASK   "255.255.255.0"                 // "AP模式子网掩码IP"
#define CFG_AP_DHCPD_GATWAY    "192.168.0.1"                   // "AP模式网关IP"
#define CFG_AP_DHCPD_DNS       "192.168.0.1"                   // "AP模式DNS IP"
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_MAC  true
//*******************************  Configure udp  ******************************************//
#define     CFG_UDP_CBNUM               10                  // "UDP socket数限值"，占用一个 tagUdpCB 结构
#define     CFG_UDP_HASHLEN             4                   // "udp socket 哈希表长度"，占用一个指针
#define     CFG_UDP_PKGMSGLEN        1472                   // udp最大包长度
#define     CFG_UDP_BUFLENDEFAULT    0x2000                 // 8KB
#define    CFG_MODULE_ENABLE_UDP           true
//*******************************  Configure tpl  ******************************************//
#define     CFG_TPL_PROTONUM            5                   // "支持的传输协议数"，占用一个 tagTplProtoItem 结构
#define    CFG_MODULE_ENABLE_TPL           true
//*******************************  Configure tcp  ******************************************//
#define     CFG_TCP_REORDER             true                                // "TCP乱序重组使能",资源充足才打开
#define     CFG_TCP_CCBNUM              15                                  // "tcp 客户端数限值"，占一个 指针 和 struct ClientCB
#define     CFG_TCP_SCBNUM              5                                   // "tcp 服务器数限值"，占一个 指针 和 struct ServerCB
#define     CFG_TCP_SOCKET_HASH_LEN     10      //用于通过“IP+port”四元组检索socket
#define    CFG_MODULE_ENABLE_TCP           true
//*******************************  Configure ftp  ******************************************//
#define     CFG_FTPD_ENABLE            false               // "ftp 服务器使能",暂未实现
#define     CFG_FTPC_ENABLE            true                // "ftp 客户端使能"
#define    CFG_MODULE_ENABLE_FTP           true
//*******************************  Configure tftp  ******************************************//
#define     CFG_TFTP_PATHDEFAULT       "/"               // TFTP默认工作目录
#define    CFG_MODULE_ENABLE_TFTP          true
//*******************************  Configure telnet  ******************************************//
#define     CFG_TELNETD_ENABLE          true                // "telnet 服务器使能"
#define     CFG_TELNETC_ENABLE          false               // "telnet 客户端使能"
#define    CFG_MODULE_ENABLE_TELNET        true
//*******************************  Configure router  ******************************************//
#define CFG_IP_STRMAX           20             // 最大路由条目数
#define    CFG_MODULE_ENABLE_ROUTER        true
//*******************************  Configure loader  ******************************************//
#define    CFG_UPDATEIBOOT_EN              false                                         // "是否支持在线更新Iboot"，
#define CFG_START_APP_IS_VERIFICATION      false                 // "启动app时是否执行校验功能"，
#define    CFG_APP_RUNMODE                 EN_DIRECT_RUN                                 // "APP运行模式",EN_DIRECT_RUN=直接从flash中运行；EN_FORM_FILE=从文件系统加载到内存运行，
#define    CFG_APP_VERIFICATION            VERIFICATION_MD5                              // "APP校验方法",
#define    CFG_IBOOT_VERSION_SMALL         00                                            // "Iboot版本号:低",xx.xx.__，APP忽略
#define    CFG_IBOOT_VERSION_MEDIUM        00                                            // "Iboot版本号:中",xx.__.xx，APP忽略
#define    CFG_IBOOT_VERSION_LARGE         01                                            // "Iboot版本号:高",__.xx.xx，APP忽略
#define    CFG_IBOOT_UPDATE_NAME           "/efs/iboot.bin"                              // "待升级iboot默认存储路径"
#define    CFG_APP_UPDATE_NAME             "/SD/app.bin"                                 // "待升级app默认存储路径"
#define    CFG_FORCED_UPDATE_PATH          "/SD/djyapp.bin"                              // "强制升级的文件路径"
#define    CFG_MODULE_ENABLE_LOADER        true
//*******************************  Configure icmp  ******************************************//
#define    CFG_MODULE_ENABLE_ICMP          true
//*******************************  Configure human machine interface  ******************************************//
#define CFG_HMIIN_DEV_LIMIT     2                   // "输入设备数量上限",人机交互输入设备数量，如键盘、鼠标等
#define    CFG_MODULE_ENABLE_HUMAN_MACHINE_INTERFACE  true
//*******************************  Configure cpu onchip adc  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_ADC  true
//*******************************  Configure stdio  ******************************************//
#define    CFG_STDIO_STDIN_MULTI           true               // "是否支持多种输入设备",
#define    CFG_STDIO_STDOUT_FOLLOW         true               // "stdout是否跟随stdin",
#define    CFG_STDIO_STDERR_FOLLOW         true               // "stderr是否跟随stdin",
#define    CFG_STDIO_FLOAT_PRINT           true               // "支持浮点打印"
#define    CFG_STDIO_STDIOFILE             true               // "支持标准IO文件"
#define    CFG_STDIO_IN_NAME               "/dev/UART2"       // "标准输入设备名",
#define    CFG_STDIO_OUT_NAME              "/dev/UART2"       // "标准输出设备名",
#define    CFG_STDIO_ERR_NAME              "/dev/UART2"       // "标准err输出设备名",
#define    CFG_MODULE_ENABLE_STDIO         true
//*******************************  Configure shell  ******************************************//
#define CFG_SHELL_STACK            0x1000                  // "执行shell命令的栈尺寸",
#define CFG_ADD_ROUTINE_SHELL      true                    // "是否添加常规shell命令",
#define CFG_ADD_EXPAND_SHELL       true                    // "是否添加拓展shell命令",
#define CFG_ADD_GLOBAL_FUN         false                   // "添加全局函数到shell",
#define CFG_SHOW_ADD_SHEELL        true                    // "显示在编译窗口添加的shell命令",
#define    CFG_MODULE_ENABLE_SHELL         true
//*******************************  Configure cpu onchip iwdt  ******************************************//
#define CFG_WDT_FEED_CYCLE          12000000                   //  "看门狗超时时间"，单位us
#define CFG_BOOT_TIME_LIMIT         30000000                 //  "启动加载超限时间",允许保护启动加载过程才需要配置此项
#define CFG_DEFEND_ON_BOOT          false                    //  "保护启动过程",启动加载过程如果出现死机，看门狗将复位，BK7251不支持，只能选false
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_IWDT  true
//*******************************  Configure cpu onchip spi  ******************************************//
#define CFG_SPI_CLK                    30000000                 // SPI 时钟。
#define CFG_SPI_CPOL                     1                     // spi时钟极性（1：SCK在空闲状态处于高电平。0：反之）。
#define CFG_SPI_CPHA                     1                     // spi时钟相位（1：在SCK周期的第二个边沿采样数据。0：在SCK周期的第一个边沿采样数据）
#define CFG_SPI_FLASH_RAM_POWER         true                   // 是否打开flash和ram的电源。
#define CFG_SPI_WORK_MODE_INTE          true                  // 设置SPI的工作模式，true为中断模式通信，false为普通模式。
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_SPI  true
//*******************************  Configure LCD driver ST7789V  ******************************************//
#define    CFG_LCD_SIZE                    32                    //  "LCD尺寸",
#define    CFG_LCD_XSIZE                   320                           //  "LCD宽度-像素数",
#define    CFG_LCD_YSIZE                   240                           //  "LCD高度-像素数",
#define    CFG_LCD_XSIZE_UM                64800                         //  "LCD宽度-微米数",
#define    CFG_LCD_YSIZE_UM                48600                         //  "LCD高度-微米数",
#define    CFG_ST7789V_DISPLAY_NAME        "lcdst7789v"                  //  "显示器名称",配置液晶显示的名称
#define    CFG_ST7789V_HEAP_NAME           "PSRAM"                       //  "驱动使用堆名",配置液晶驱动所使用的堆名称
#define    CFG_MODULE_ENABLE_LCD_DRIVER_ST7789V  true
//*******************************  Configure graphical decorate development  ******************************************//
#define CFG_DESKTOP_WIDTH       0                      // "桌面宽度",桌面尺寸（像素数）宽度，0=显示器宽度
#define CFG_DESKTOP_HEIGHT      0                      // "桌面高度",桌面尺寸（像素数）高度，0=显示器高度
#define CFG_DESKTOP_BUF         false                   // "桌面窗口是否带缓存"，内存稀少的硬件可不带缓存
#define CFG_DISPLAY_NAME        "lcdst7789v"                    // "显示器名",须与bsp中显示器驱动模块配置的显示器名字相同
#define CFG_DESKTOP_NAME        "desktop"                    // "给桌面起个名字"，
#define CFG_DESKTOP_FORMAT      CN_SYS_PF_RGB565               // "像素格式",桌面窗口像素格式，常数在gkernel.h中定义，一般使用与显示器相同颜色
#define CFG_GRAY_BASE_COLOR     CN_COLOR_WHITE                 // "灰度基色",像素格式设为灰度时才需要设置的“最亮”色，可在gkernel.h中找到常用颜色定义
#define CFG_FILL_COLOR          CN_COLOR_BLUE                  // "填充色",创建桌面时的填充色，用888格式，可在gkernel.h中找到常用颜色定义
#define    CFG_MODULE_ENABLE_GRAPHICAL_DECORATE_DEVELOPMENT  true
//*******************************  Configure touch  ******************************************//
#define    CFG_MODULE_ENABLE_TOUCH         true
//*******************************  Configure touchscreen FT6236  ******************************************//
#define    CFG_TOUCH_SIZE                  32               //  "触摸尺寸",
#define    CT_MAX_TOUCH                    5                        //  "触控数",支持最多5点触摸
#define    CFG_TOUCH_ADJUST_FILE           "/efs/touch_init.dat"         //  "矫正文件路径",保存触摸屏矫正参数的文件
#define    CFG_FT6236_BUS_NAME             "IoIic"                  //  "IIC总线名称",触摸芯片使用的IIC总线名称
#define    CFG_FT6236_TOUCH_NAME           "FT6236"                 //  "触摸屏名称",配置触摸屏名称
#define    CFG_TARGET_DISPLAY_NAME         "lcdst7789v"             //  "触屏所在显示器名称",配置触摸屏所在显示器的名称
#define    CFG_MODULE_ENABLE_TOUCHSCREEN_FT6236  true
//*******************************  Configure key board  ******************************************//
#define    CFG_MODULE_ENABLE_KEY_BOARD     true
//*******************************  Configure keyboard hard driver  ******************************************//
#define CFG_KEYBOARD_NAME              "KEYBOARD"              //  "键盘名",配置键盘名称
#define    CFG_MODULE_ENABLE_KEYBOARD_HARD_DRIVER  true
//*******************************  Core Clock  ******************************************//
#define    CFG_CORE_MCLK                   (180.0*Mhz)       //主频，内核要用，必须定义
//*******************************  DjyosProduct Configuration  ******************************************//
#define    PRODUCT_MANUFACTURER_NAME       "深圳市创联时代科技有限公司"            //厂商名称
#define    PRODUCT_PRODUCT_CLASSIFY        "健康盒子"        //产品分类
#define    PRODUCT_PRODUCT_MODEL           "JK-1"            //产品型号
#define    PRODUCT_VERSION_LARGE           0                 //版本号,__.xx.xx
#define    PRODUCT_VERSION_MEDIUM          4                 //版本号,xx.__.xx
#define    PRODUCT_VERSION_SMALL           1                 //版本号,xx.xx.__
#define    PRODUCT_PRODUCT_MODEL_CODE      "JIUP2P"          //产品型号编码
#define    PRODUCT_PASSWORD                "002"             //产品秘钥
#define    PRODUCT_OTA_ADDRESS             "factory.cpolar.cn"    //OTA服务器地址
#define    PRODUCT_BOARD_TYPE              "HealthBox"       //板件类型
#define    PRODUCT_CPU_TYPE                "bk7251"          //CPU类型


#endif
