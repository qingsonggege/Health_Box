
//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
#include <cpu_peri_adc.h>
#include <djyos.h>
#include <typedef.h>
#include <gpio_pub.h>
#include <cpu_peri_gpio.h>

//extern void deep_sleep(void);
//extern int Get_StabilizeVol(void);
//extern UINT32 usb_is_plug_in(void);
#define FULL_VOL_DIFF   4       //用于判断充满电的采集电压差
char vol_to_percentage(int assign)
{
    int vol = 0,vol_diff = FULL_VOL_DIFF;
    bool_t charge_start = false;
    static char percentage = 0;
    static char critical = 100,num = 0;
    static int vol_temp = 0,vol_last = 0;
    if(assign == 0)
    {
//        if(usb_is_plug_in() != 0)
//        {
//            vol = Get_StabilizeVol();
//        }
//        else
//        {
//            vol = vbat_voltage_get();
//        }

        vol = vbat_voltage_get();
        if(djy_gpio_read(GPIO34))
        {
            charge_start = true;

            if(vol < 3800)
                vol -= 230;
            else if(vol < 3920)
                vol -= 200;
            else if(vol < 4020)
                vol -= 170;
            else if(vol < 4070)
                vol -= 150;
            else if(vol < 4100)
                vol -= 120;
            else if(vol < 4110)
                vol -= 80;
            else if(vol < 4300)
                vol -= 60;
        }

        printf("测量到的Vbat = %d  \r\n", vol);    //显示当前的电池电压
    }
    else
    {
        vol = assign;
        printf("指定的Vbat = %d  \r\n", vol);    //显示当前的电池电压
    }
    if(vol >= 3850)
    {
        if(critical < 100)
        {
            if(charge_start == true)
            {
                printf(" =====  vol(%d) = %d ======\r\n",num,vol);

                vol_temp += vol;
                num++;
                if(num >= 10)
                {
                    vol_temp = vol_temp / num;
                    vol_diff = vol_temp - vol_last;
                    printf(" =====  vol_diff = %d ======\r\n",vol_diff);
                    num = 0;
                    vol_last = vol_temp;
                    vol_temp = 0;
                }
            }
            else
            {
                num = 0;
//                memset(vol_temp, 0, 10);
                vol_temp = 0;
                vol_last = 0;
                if(vol >= 3860)
                {
                    percentage = 100;
                    critical = 100;
                    printf("%s ,%d,percentage = %d\r\n",__func__,__LINE__,percentage);
                    return percentage;
                }
            }
            if((vol > 4120) || (vol_diff < FULL_VOL_DIFF))
            {
                percentage = 100;
                critical = 100;
                printf("%s ,%d,percentage = %d\r\n",__func__,__LINE__,percentage);
                return percentage;
            }
        }
        else
        {
            percentage = 100;
            critical = 100;
            printf("%s ,%d,percentage = %d\r\n",__func__,__LINE__,percentage);
            return percentage;
        }
    }
//    else if(vol >= 3790)
    if(vol >= 3700)
    {
        if(critical < 75)
        {
            if(charge_start)
            {
                if(vol > 3900)
                {
                    percentage = 75;
                    critical = 75;
                    printf("%s ,%d,percentage = %d\r\n",__func__,__LINE__,percentage);
                    return percentage;
                }
            }
            else
            {
                if(vol > 3710)
                {
                    percentage = 75;
                    critical = 75;
                    printf("%s ,%d,percentage = %d\r\n",__func__,__LINE__,percentage);
                    return percentage;
                }
            }
        }
        else
        {
            percentage = 75;
            critical = 75;
            printf("%s ,%d,percentage = %d\r\n",__func__,__LINE__,percentage);
            return percentage;
        }
    }

    if(vol >= 3580)
    {
        if(critical < 50)
        {
            if(charge_start)
            {
                if(vol > 3680)
                {
                    percentage = 50;
                    critical = 50;
                    printf("%s ,%d,percentage = %d\r\n",__func__,__LINE__,percentage);
                    return percentage;
                }
            }
            else
            {
                if(vol > 3590)
                {
                    percentage = 50;
                    critical = 50;
                    printf("%s ,%d,percentage = %d\r\n",__func__,__LINE__,percentage);
                    return percentage;
                }
            }
        }
        else
        {
            percentage = 50;
            critical = 50;
            printf("%s ,%d,percentage = %d\r\n",__func__,__LINE__,percentage);
            return percentage;
        }
    }

//    else if(vol >= 3720)
    if(vol >= 3450)
    {
        if(critical < 25)
        {
            if(charge_start)
            {
                if(vol > 3560)
                {
                    percentage = 25;
                    critical = 25;
                    printf("%s ,%d,percentage = %d\r\n",__func__,__LINE__,percentage);
                    return percentage;
                }
            }
            else
            {
                if(vol > 3460)
                {
                    percentage = 25;
                    critical = 25;
                    printf("%s ,%d,percentage = %d\r\n",__func__,__LINE__,percentage);
                    return percentage;
                }
            }
        }
        else
        {
            percentage = 25;
            critical = 25;
            printf("%s ,%d,percentage = %d\r\n",__func__,__LINE__,percentage);
            return percentage;
        }
    }
    percentage = 1;
    critical = 1;

//    else if(vol >= 3630)
//    if(vol >= 3430)
//    {
//        if(critical < 25)
//        {
//            if(vol > 3560)
//            {
//                percentage = 25;
//                critical = 25;
//                return percentage;
//            }
//        }
//        else
//        {
//            percentage = 25;
//            critical = 25;
//            return percentage;
//        }
//    }
//    if(vol >= 3590)
//    {
//        percentage = 1;
//    }
//    else
    if((vol < 3310) && (charge_start == false))
    {
        if(vol >= 2700)     //防止一些异常电压导致休眠
        {
            printf("电量不足，准备关机\r\n");
//            DJY_EventDelay(3000*1000);
            percentage = 0;
            Set_ShoutDown();
        }
        else
        {
            percentage = -1; //异常电压，返回这个，电量显示不做响应。
        }
    }
    printf("%s ,%d,percentage = %d,vol = %d\r\n",__func__,__LINE__,percentage,vol);
    return percentage;

}
